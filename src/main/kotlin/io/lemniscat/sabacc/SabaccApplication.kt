package io.lemniscat.sabacc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SabaccApplication

fun main(args: Array<String>) {
	runApplication<SabaccApplication>(*args)
}
