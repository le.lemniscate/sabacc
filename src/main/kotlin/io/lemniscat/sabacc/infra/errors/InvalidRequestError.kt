package io.lemniscat.sabacc.infra.errors

import io.lemniscat.sabacc.domain.error.GameError

class InvalidRequestError: GameError
