package io.lemniscat.sabacc.infra.dto

import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.player.Player

data class OpponentProjection(
        override val id: String,
        override val name: String,
        override val credits: Int,
        override val status: String,
        override val interferenceField: List<CardDTO>,
        val handSize: Int
): PlayerProjection {
    companion object Factories {
        fun from(player: Player) = OpponentProjection(
                id = player.id.value,
                name = player.name.value,
                credits = player.credits,
                status = player.status.render(),
                interferenceField = player.interferenceField.cards.map(CardDTO.Factories::from),
                handSize = player.hand.cards.size
        )
    }
}