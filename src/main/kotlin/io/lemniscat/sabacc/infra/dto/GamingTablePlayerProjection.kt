package io.lemniscat.sabacc.infra.dto

import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.phases.*

data class GamingTablePlayerProjection(
        val id: String,
        val player: FullPlayerProjection,
        val opponents: List<PlayerProjection>,
        val round: Int,
        val phase: String,
        val currentPlayer: String?,
        val startingPlayer: String?,
        val currentBet: Int?,
        val mainPot: Int,
        val sabaccPot: Int,
        val ruleset: String,
        val deck: DeckProjection
) {
    companion object Factories {
        fun project(origin: GamingTable, player: Id): GamingTablePlayerProjection =
            when (origin.phase) {
                is SuddenDemisePhase -> project(origin, player, false)
                is GameOver -> project(origin, player, false)
                else -> project(origin, player, true)
            }

        private fun project(origin: GamingTable, playerId: Id, hideOpponents: Boolean) = GamingTablePlayerProjection(
                id = origin.id.value,
                player = FullPlayerProjection.from(origin.get(playerId)),
                opponents = origin.players.content.fold(listOf()) { acc, player ->
                    if (player.id == playerId) {
                        acc
                    } else {
                        if (hideOpponents) {
                            acc + OpponentProjection.from(player)
                        } else {
                            acc + FullPlayerProjection.from(player)
                        }
                    }
                },
                round = origin.round,
                phase = origin.phase.name.name,
                currentPlayer = if (origin.phase is TurnPhase) {
                    origin.phase.currentPlayer.value
                } else {
                    null
                },
                startingPlayer = if (origin.phase is TurnPhase) {
                    origin.phase.currentPlayer.value
                } else {
                    null
                },
                mainPot = origin.mainPot.credits,
                sabaccPot = origin.sabaccPot.credits,
                ruleset = origin.ruleset.render(),
                deck = DeckProjection.from(origin.deck),
                currentBet = if (origin.phase is BettingPhase) {
                    origin.phase.currentBet
                } else {
                    null
                }
        )
    }
}