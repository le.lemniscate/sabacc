package io.lemniscat.sabacc.infra.dto

interface PlayerProjection {
    val id: String
    val name: String
    val credits: Int
    val status: String
    val interferenceField: List<CardDTO>
}