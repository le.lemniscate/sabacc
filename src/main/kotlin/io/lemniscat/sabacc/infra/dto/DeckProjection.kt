package io.lemniscat.sabacc.infra.dto

import io.lemniscat.sabacc.domain.model.card.Deck

data class DeckProjection(val size: Int) {
    companion object Factories {
        fun from(deck: Deck) = DeckProjection(deck.cards.size)
    }
}