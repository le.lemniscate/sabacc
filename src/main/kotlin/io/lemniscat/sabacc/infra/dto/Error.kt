package io.lemniscat.sabacc.infra.dto

import io.lemniscat.sabacc.domain.error.GameError
import io.lemniscat.sabacc.domain.error.LobbyError

data class Error(val value: String) {
    companion object Factories {
        fun from(gameError: GameError) =
                Error(gameError.javaClass.name)

        fun from(lobbyError: LobbyError) =
                Error(lobbyError.javaClass.name)
    }

    fun render(): String = value
}