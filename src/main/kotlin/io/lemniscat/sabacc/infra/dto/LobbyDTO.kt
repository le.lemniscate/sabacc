package io.lemniscat.sabacc.infra.dto

import io.lemniscat.sabacc.domain.model.Lobby

data class LobbyDTO(
        val id: String,
        val users: List<UserDTO>,
        val allowance: Int
) {
    companion object Factories {
        fun from(lobby: Lobby) = LobbyDTO(
                lobby.id.value,
                lobby.users.map { UserDTO.from(it) },
                lobby.allowance
        )
    }
}