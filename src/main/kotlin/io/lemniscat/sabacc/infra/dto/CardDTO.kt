package io.lemniscat.sabacc.infra.dto

import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.card.SuitCard

const val SUIT = "SUIT"
const val FACE = "FACE"

data class CardDTO(
        val suit: String,
        val name: String,
        val value: Int,
        val type: String
) {
    companion object Factories {
        fun from(card: Card): CardDTO = CardDTO(
                card.suit.name,
                card.value.name,
                card.value.points,
                if (card is SuitCard) {
                    SUIT
                } else {
                    FACE
                }
        )
    }
}