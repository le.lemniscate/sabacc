package io.lemniscat.sabacc.infra.dto

import io.lemniscat.sabacc.domain.model.User

class UserDTO(
        val id: String,
        val name: String
) {
    companion object Factories {
        fun from(user: User) = UserDTO(user.id.value, user.name.value)
    }
}