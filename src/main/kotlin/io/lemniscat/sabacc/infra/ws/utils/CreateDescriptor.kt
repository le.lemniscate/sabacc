package io.lemniscat.sabacc.infra.ws.utils

import io.lemniscat.sabacc.infra.ws.handlers.WebSocketAction
import io.lemniscat.sabacc.infra.ws.handlers.OutputMessage
import io.lemniscat.sabacc.infra.ws.handlers.InputMessage

inline fun <reified T> createDescriptor(noinline handler: (InputMessage<T>) -> OutputMessage) where T: WebSocketAction
        = WebSocketActionDescriptor(T::class.java, handler as (InputMessage<WebSocketAction>) -> OutputMessage)
