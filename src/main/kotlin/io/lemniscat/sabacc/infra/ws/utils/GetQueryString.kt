package io.lemniscat.sabacc.infra.ws.utils

import java.net.URI

fun URI.getQueryString(): Map<String, String> =
    this.query.split("&")
            .map {
                val pair = it.split("=")
                pair[0] to pair[1]
            }.toMap()