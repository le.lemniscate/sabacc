package io.lemniscat.sabacc.infra.ws.handlers

import io.lemniscat.sabacc.domain.error.InvalidStateForCalledAction
import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.phases.Revealing
import io.lemniscat.sabacc.domain.model.phases.SuddenDemisePhase
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import org.springframework.stereotype.Component

@Component
class GameFlowWSHandler(
        private val gamingTableRepository: GamingTableRepository
) {
    fun readyForSuddenDemise(request: InputMessage<ReadyForSuddenDemise>): OutputMessage =
            retrieveTable(request) {
                if (it.phase is SuddenDemisePhase) {
                    OutputMessage.of(PlayerMessageTypes.READY_FOR_SUDDEN_DEMISE, it, request.requester.userId)
                } else {
                    OutputMessage.of(InvalidStateForCalledAction(), request.requester.userId)
                }
            }

    fun readyForReveal(request: InputMessage<ReadyForReveal>): OutputMessage =
            retrieveTable(request) {
                if (it.phase is Revealing) {
                    OutputMessage.of(PlayerMessageTypes.READY_FOR_REVEAL, it, request.requester.userId)
                } else {
                    OutputMessage.of(InvalidStateForCalledAction(), request.requester.userId)
                }
            }

    private fun <T> retrieveTable(request: InputMessage<T>, callback: (GamingTable) -> OutputMessage): OutputMessage where T: GameAction =
            (gamingTableRepository retrieve Id.of(request.action.gameId))
                    .map(callback)
                    .getOrElseGet {
                        OutputMessage.of(it, request.requester.userId)
                    }

}
