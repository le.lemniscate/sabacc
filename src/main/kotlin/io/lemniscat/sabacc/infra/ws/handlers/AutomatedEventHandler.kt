package io.lemniscat.sabacc.infra.ws.handlers

import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.lemniscat.sabacc.domain.usecases.game.RevealCards
import io.lemniscat.sabacc.domain.usecases.game.SuddenDemise
import org.springframework.stereotype.Component

@Component
class AutomatedEventHandler(
        private val gamingTableRepository: GamingTableRepository
) {
    fun suddenDemise(gamingTable: GamingTable): OutputMessage =
            SuddenDemise(gamingTableRepository)(gamingTable)
                    .map {
                        OutputMessage.of(GameMessageTypes.SUDDEN_DEMISE, it)
                    }.getOrElseGet {
                        OutputMessage.of(it)
                    }

    fun reveal(gamingTable: GamingTable): OutputMessage =
            RevealCards(gamingTableRepository)(gamingTable)
                    .map {
                        OutputMessage.of(GameMessageTypes.REVEAL, it)
                    }.getOrElseGet {
                        OutputMessage.of(it)
                    }
}