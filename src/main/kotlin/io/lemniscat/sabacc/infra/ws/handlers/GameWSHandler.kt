package io.lemniscat.sabacc.infra.ws.handlers

import io.lemniscat.sabacc.domain.error.GameError
import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.card.*
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.lemniscat.sabacc.domain.usecases.game.*
import io.lemniscat.sabacc.infra.dto.FACE
import io.lemniscat.sabacc.infra.dto.SUIT
import io.lemniscat.sabacc.infra.errors.InvalidRequestError
import io.vavr.control.Either
import org.springframework.stereotype.Component

fun Card.Factories.from(payload: CardAction): Either<out GameError, Card> =
        when (payload.type) {
            SUIT -> {
                try {
                    Either.right<GameError, Card>(SuitCard.of(StandardSuit.valueOf(payload.suit), SuitValue.valueOf(payload.name)))
                } catch (e: IllegalArgumentException) {
                    Either.left<GameError, Card>(InvalidRequestError())
                }
            }
            FACE -> {
                try {
                    Either.right<GameError, Card>(FaceCard.of(FaceSuit.valueOf(payload.suit), FaceValue.valueOf(payload.name)))
                } catch (e: IllegalArgumentException) {
                    Either.left<GameError, Card>(InvalidRequestError())
                }
            }
            else -> Either.left(InvalidRequestError())
        }

@Component
class GameWSHandler(
        private val gamingTableRepository: GamingTableRepository
) {
    fun joinGame(request: InputMessage<GetGame>): OutputMessage =
            retrieveTable(request) {
                OutputMessage.of(GameMessageTypes.GAME_JOINED, it, request.requester.userId)
            }


    fun call(request: InputMessage<CallHand>): OutputMessage =
            retrieveTable(request) {
                Call(gamingTableRepository)(it, request.requester.userId)
                        .map { table ->
                            OutputMessage.of(GameMessageTypes.HAND_CALLED, table, request.requester.userId)
                        }.getOrElseGet { error ->
                            OutputMessage.of(error, request.requester.userId)
                        }
            }

    fun draw(request: InputMessage<DrawCard>): OutputMessage =
            retrieveTable(request) {
                Draw(gamingTableRepository)(it, request.requester.userId)
                        .map { table ->
                            OutputMessage.of(GameMessageTypes.CARD_DREW, table, request.requester.userId)
                        }.getOrElseGet { error ->
                            OutputMessage.of(error, request.requester.userId)
                        }
            }

    fun fold(request: InputMessage<PlayerFold>): OutputMessage =
            retrieveTable(request) {
                Fold(gamingTableRepository)(it, request.requester.userId)
                        .map { table ->
                            OutputMessage.of(GameMessageTypes.FOLDED, table, request.requester.userId)
                        }.getOrElseGet { error ->
                            OutputMessage.of(error, request.requester.userId)
                        }
            }

    fun matchBet(request: InputMessage<PlayerMatchBet>): OutputMessage =
            retrieveTable(request) {
                MatchBet(gamingTableRepository)(it, request.requester.userId)
                        .map { table ->
                            println("Match bet. Table: $table")
                            OutputMessage.of(GameMessageTypes.BET_MATCHED, table, request.requester.userId)
                        }.getOrElseGet { error ->
                            OutputMessage.of(error, request.requester.userId)
                        }
            }

    fun putInInterferenceField(request: InputMessage<PutCardInInterferenceField>): OutputMessage =
            retrieveCard(request) { card ->
            retrieveTable(request) {
                PutInInterferenceField(gamingTableRepository)(
                    it,
                    request.requester.userId,
                    card
                )
                    .map { table ->
                        OutputMessage.of(GameMessageTypes.PUT_IN_INTERFERENCE_FIELD, table, request.requester.userId)
                    }.getOrElseGet { error ->
                        OutputMessage.of(error, request.requester.userId)
                    }
            }}

    fun removeFromInterferenceField(request: InputMessage<RemoveCardFromInterferenceField>): OutputMessage =
            retrieveCard(request) { card ->
            retrieveTable(request) {
                RemoveFromInterferenceField(gamingTableRepository)(
                        it,
                        request.requester.userId,
                        card
                )
                    .map { table ->
                        OutputMessage.of(GameMessageTypes.REMOVED_FROM_INTERFERENCE_FIELD, table, request.requester.userId)
                    }.getOrElseGet { error ->
                        OutputMessage.of(error, request.requester.userId)
                    }
            }}

    fun trade(request: InputMessage<TradeCard>): OutputMessage =
            retrieveCard(request) { card ->
            retrieveTable(request) {
                Trade(gamingTableRepository)(it, request.requester.userId, card)
                        .map { table ->
                            OutputMessage.of(GameMessageTypes.CARD_TRADED, table, request.requester.userId)
                        }.getOrElseGet { error ->
                            OutputMessage.of(error, request.requester.userId)
                        }
            }}

    fun raise(request: InputMessage<PlayerRaiseBet>): OutputMessage =
            retrieveTable(request) {
                RaiseBet(gamingTableRepository)(it, request.requester.userId, request.action.amount)
                        .map { table ->
                            println("New table: $table")
                            OutputMessage.of(GameMessageTypes.BET_RAISED, table, request.requester.userId)
                        }.getOrElseGet { error ->
                            OutputMessage.of(error, request.requester.userId)
                        }
            }

    fun stand(request: InputMessage<PlayerStand>): OutputMessage =
            retrieveTable(request) {
                Stand(gamingTableRepository)(it, request.requester.userId)
                        .map { table ->
                            OutputMessage.of(GameMessageTypes.STOOD, table, request.requester.userId)
                        }.getOrElseGet { error ->
                            OutputMessage.of(error, request.requester.userId)
                        }
            }

    private fun <T> retrieveTable(request: InputMessage<T>, callback: (GamingTable) -> OutputMessage): OutputMessage where T: GameAction =
            (gamingTableRepository retrieve Id.of(request.action.gameId))
                    .map(callback)
                    .getOrElseGet {
                        OutputMessage.of(it, request.requester.userId)
                    }

    private fun <T> retrieveCard(request: InputMessage<T>, callback: (Card) -> OutputMessage): OutputMessage where T: CardAction =
            Card.from(request.action).map(callback).getOrElseGet {
                OutputMessage.of(it, request.requester.userId)
            }
}
