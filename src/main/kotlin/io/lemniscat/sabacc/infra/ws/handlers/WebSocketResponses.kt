package io.lemniscat.sabacc.infra.ws.handlers

import io.lemniscat.sabacc.domain.error.GameError
import io.lemniscat.sabacc.domain.error.LobbyError
import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.Lobby
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.infra.dto.GamingTablePlayerProjection
import io.lemniscat.sabacc.infra.dto.LobbyDTO
import io.lemniscat.sabacc.infra.ws.UserSession

const val LOBBY_ERROR = "LOBBY_ERROR"
const val GAME_ERROR = "GAME_ERROR"

enum class LobbyMessageTypes(val typeForCaller: String, val typeForOther: String) {
    LOBBY_CREATED("LOBBY_CREATED", "LOBBY_CREATED"),
    LOBBY_JOINED("LOBBY_JOINED", "PLAYER_JOINED_LOBBY"),
    LOBBY_LEFT("LOBBY_LEFT", "PLAYER_LEFT_LOBBY")
}

enum class GameMessageTypes(val typeForCaller: String, val typeForOther: String) {
    GAME_LAUNCHED("GAME_LAUNCHED", "PLAYER_LAUNCHED_GAME"),
    GAME_JOINED("GAME_JOINED", "PLAYER_JOINED_GAME"),
    HAND_CALLED("HAND_CALLED", "PLAYER_CALLED_HAND"),
    CARD_DREW("CARD_DREW", "PLAYER_DREW_CARD"),
    FOLDED("FOLDED", "PLAYER_FOLDED"),
    BET_MATCHED("BET_MATCHED", "PLAYER_MATCHED_BET"),
    PUT_IN_INTERFERENCE_FIELD("CARD_PUT_IN_FIELD", "PLAYER_PUT_CARD_IN_FIELD"),
    BET_RAISED("BET_RAISED", "PLAYER_RAISED_BET"),
    REMOVED_FROM_INTERFERENCE_FIELD("CARD_REMOVED_FROM_FIELD", "PLAYER_REMOVED_CARD_FROM_FIELD"),
    STOOD("STOOD", "PLAYER_STOOD"),
    CARD_TRADED("CARD_TRADED", "PLAYER_TRADED_CARD"),

    SUDDEN_DEMISE("SUDDEN_DEMISE", "SUDDEN_DEMISE"),
    REVEAL("REVEAL", "REVEAL"),

    SABACC_SHIFT("SABACC_SHIFT", "SABACC_SHIFT"),
    GAME_OVER("GAME_OVER", "GAME_OVER")
}

enum class PlayerMessageTypes(val typeForCaller: String, val typeForOther: String) {
    READY_FOR_SUDDEN_DEMISE("READY_FOR_SUDDEN_DEMISE", "PLAYER_READY_FOR_SUDDEN_DEMISE"),
    READY_FOR_REVEAL("READY_FOR_REVEAL", "PLAYER_READY_FOR_REVEAL")
}

data class WebSocketResponse(
        val sessionId: String, // TODO: remove?!
        val type: String,
        val payload: Any
)

data class InputMessage<T: WebSocketAction>(
        val requester: UserSession,
        val action: T
)

interface OutputMessage {
    companion object Factories {
        fun of(type: LobbyMessageTypes, lobby: Lobby, caller: Id): OutputMessage = LobbyMessage(type, lobby, caller)
        fun of(type: GameMessageTypes, table: GamingTable, caller: Id): OutputMessage = GamingTableMessage(type, table, caller)
        fun of(type: GameMessageTypes, table: GamingTable): OutputMessage = BroadcastGamingTable(type, table)
        fun of(type: PlayerMessageTypes, gamingTable: GamingTable, caller: Id): OutputMessage = PlayerMessage(type, caller, caller, gamingTable)

        fun of(lobbyError: LobbyError, userId: Id): OutputMessage = ErrorMessage(LOBBY_ERROR, lobbyError.toString(), userId)
        fun of(gameError: GameError, userId: Id) = ErrorMessage(GAME_ERROR, gameError.toString(), userId)
        fun of(errorMessage: String, userId: Id) = ErrorMessage("UNKNOWN_ACTION", errorMessage, userId)
        fun of(gameError: GameError) = BroadcastErrorMessage(GAME_ERROR, gameError.toString())
    }

    fun project(sessionId: String, id: Id): WebSocketResponse
}

data class PlayerMessage(
        val type: PlayerMessageTypes,
        val target: Id,
        val caller: Id,
        val gamingTable: GamingTable
): OutputMessage {
    override fun project(sessionId: String, id: Id): WebSocketResponse =
            if (id == caller) {
                WebSocketResponse(sessionId, type.typeForCaller, target)
            } else {
                WebSocketResponse(sessionId, type.typeForOther, target)
            }
}

data class LobbyMessage(
        val type: LobbyMessageTypes,
        val content: Lobby,
        val caller: Id
): OutputMessage {
    override fun project(sessionId: String, id: Id): WebSocketResponse =
            if (id == caller) {
                WebSocketResponse(sessionId, type.typeForCaller, LobbyDTO.from(content))
            } else {
                WebSocketResponse(sessionId, type.typeForOther, LobbyDTO.from(content))
            }
}

interface IGamingTableMessage: OutputMessage {
    val type: GameMessageTypes
    val content: GamingTable
}

data class GamingTableMessage(
        override val type: GameMessageTypes,
        override val content: GamingTable,
        val caller: Id
): IGamingTableMessage {
    override fun project(sessionId: String, id: Id): WebSocketResponse =
            if (id == caller) {
                WebSocketResponse(sessionId, type.typeForCaller, GamingTablePlayerProjection.project(content, id))
            } else {
                WebSocketResponse(sessionId, type.typeForOther, GamingTablePlayerProjection.project(content, id))
            }
}

data class BroadcastGamingTable( // TODO: can certainly be merged with the gamingTableMessage (by switching the GameMEssageTypes in two subtypes?)
        override val type: GameMessageTypes,
        override val content: GamingTable
): IGamingTableMessage {
    override fun project(sessionId: String, id: Id): WebSocketResponse =
            WebSocketResponse(sessionId, type.typeForCaller, GamingTablePlayerProjection.project(content, id))
}

data class ErrorMessage(
        val type: String,
        val content: String,
        val id: Id
): OutputMessage {
    override fun project(sessionId: String, id: Id): WebSocketResponse =
            WebSocketResponse(sessionId, type, content)
}

data class BroadcastErrorMessage(
        val type: String,
        val content: String
): OutputMessage {
    override fun project(sessionId: String, id: Id): WebSocketResponse =
            WebSocketResponse(sessionId, type, content)
}
