package io.lemniscat.sabacc.infra.ws.utils

import io.lemniscat.sabacc.infra.ws.*
import io.lemniscat.sabacc.infra.ws.handlers.*
import org.reactivestreams.Publisher
import org.reactivestreams.Subscriber
import org.springframework.stereotype.Component
import reactor.core.publisher.DirectProcessor
import java.util.function.Consumer

@Component
class SabaccPubSub(
        val router: SabaccWSRouter,
        val automatedEventHandler: AutomatedEventHandler
): Consumer<WebSocketRequest>, Publisher<WebSocketResponse> {
    val sabaccUsers = SabaccUsers()

    val fluxProcessor = DirectProcessor.create<WebSocketResponse>().serialize()
    // TODO: can remove the fact that we implement a lot of things, and then do a map on the fluxProcessor to transform the message toward a result
    val sink = fluxProcessor.sink()

    override fun subscribe(s: Subscriber<in WebSocketResponse>) {
        fluxProcessor.subscribe(s)
    }

    override fun accept(message: WebSocketRequest) {
        sabaccUsers.register(message.userId, message.sessionId)
        send(
            router.route(
                sabaccUsers.getSession(message.sessionId),
                message
            )
        )
    }

    private fun send(message: OutputMessage) {
        when (message) {
            is IGamingTableMessage -> {
                message.content.players.content.forEach {
                    if (!it.stillInGame) {
                        sabaccUsers.outOfGame(it.id)
                    }
                    sink.next(message.project(sabaccUsers.getSession(it.id).sessionId, it.id)) // TODO: we can do this in the filter of the SabaccWebSocketHandler (and instead of filter, map)
                }
            }
            is LobbyMessage -> {
                message.content.users.forEach {
                    sink.next(message.project(sabaccUsers.getSession(it.id).sessionId, it.id))
                }
            }
            is PlayerMessage -> {
                when (message.type) {
                    PlayerMessageTypes.READY_FOR_REVEAL -> {
                        val table = message.gamingTable
                        val players = table.players.content
                        players.forEach {
                            sink.next(message.project(sabaccUsers.getSession(it.id).sessionId, it.id))
                        }
                        println("SabaccUsers status: ${sabaccUsers.idToFlowStatus}")
                        sabaccUsers.waitForReveal(message.target)
                        if (sabaccUsers.areAllWaitingForReveal(players)) {
                            send(automatedEventHandler.reveal(table))
                        }
                    }
                    PlayerMessageTypes.READY_FOR_SUDDEN_DEMISE -> {
                        val table = message.gamingTable
                        val players = table.players.content
                        players.forEach {
                            sink.next(message.project(sabaccUsers.getSession(it.id).sessionId, it.id))
                        }
                        sabaccUsers.waitForSuddenDemise(message.target)
                        if (sabaccUsers.areAllWaitingForSuddenDemise(players)) {
                            send(automatedEventHandler.suddenDemise(table))
                        }
                    }
                }
            }
            is ErrorMessage -> {
                sink.next(message.project(sabaccUsers.getSession(message.id).sessionId, message.id))
            }
        }
    }
}
