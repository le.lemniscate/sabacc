package io.lemniscat.sabacc.infra.ws.handlers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.infra.ws.UserSession
import io.lemniscat.sabacc.infra.ws.utils.WebSocketActionDescriptor
import io.lemniscat.sabacc.infra.ws.utils.createDescriptor
import org.springframework.stereotype.Component
import org.springframework.web.reactive.socket.WebSocketMessage

data class WebSocketRequest (
        val webSocketMessage: WebSocketMessage,
        val sessionId: String,
        val userId: Id
)

@Component
class SabaccWSRouter(
        lobbyWSHandler: LobbyWSHandler,
        gameWSHandler: GameWSHandler,
        gameFlowHandler: GameFlowWSHandler
) {
    val wsActionFactories: Map<String, WebSocketActionDescriptor> = mapOf(
            "CREATE_LOBBY" to createDescriptor(lobbyWSHandler::create),
            "JOIN_LOBBY" to createDescriptor(lobbyWSHandler::join),
            "LEAVE_LOBBY" to createDescriptor(lobbyWSHandler::leave),
            "LAUNCH_GAME" to createDescriptor(lobbyWSHandler::launch),

            "JOIN_GAME" to createDescriptor(gameWSHandler::joinGame),
            "CALL" to createDescriptor(gameWSHandler::call),
            "DRAW" to createDescriptor(gameWSHandler::draw),
            "FOLD" to createDescriptor(gameWSHandler::fold),
            "MATCH_BET" to createDescriptor(gameWSHandler::matchBet),
            "PUT_IN_FIELD" to createDescriptor(gameWSHandler::putInInterferenceField),
            "REMOVE_FROM_FIELD" to createDescriptor(gameWSHandler::removeFromInterferenceField),
            "TRADE" to createDescriptor(gameWSHandler::trade),
            "RAISE" to createDescriptor(gameWSHandler::raise),
            "STAND" to createDescriptor(gameWSHandler::stand),

            "READY_FOR_REVEAL" to createDescriptor(gameFlowHandler::readyForReveal),
            "READY_FOR_SUDDEN_DEMISE" to createDescriptor(gameFlowHandler::readyForSuddenDemise)
    )

    fun route(requester: UserSession, message: WebSocketRequest): OutputMessage {
        val objectMapper = ObjectMapper()
        val payload = message.webSocketMessage.payloadAsText
        return try {
            val action: ObjectNode = objectMapper.readValue(payload, ObjectNode::class.java)
            if (action.has("type") && action.has("payload")) {
                val prettyAction = action.get("type").toString().filter { it != '"' }
                val descriptor = wsActionFactories[prettyAction]
                if (descriptor != null) {
                    descriptor.handler(wsRequest(requester, action, descriptor))
                } else {
                    OutputMessage.of(prettyAction, requester.userId)
                }
            } else {
                OutputMessage.of("Type not defined or payload not defined", requester.userId)
            }
        } catch(e: JsonProcessingException) {
            OutputMessage.of(e.originalMessage, requester.userId)
        }
    }

    private fun wsRequest(requester: UserSession, action: ObjectNode, descriptor: WebSocketActionDescriptor): InputMessage<WebSocketAction> {
        return InputMessage(
                requester,
                ObjectMapper()
                        .registerModule(KotlinModule())
                        .readValue(action.get("payload").toString(), descriptor.actionClass)
        )
    }
}