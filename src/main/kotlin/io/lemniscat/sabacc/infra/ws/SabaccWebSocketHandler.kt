package io.lemniscat.sabacc.infra.ws

import com.fasterxml.jackson.databind.ObjectMapper
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.player.Player
import io.lemniscat.sabacc.domain.model.player.Players
import io.lemniscat.sabacc.infra.ws.handlers.PlayerMessageTypes
import io.lemniscat.sabacc.infra.ws.handlers.WebSocketRequest
import io.lemniscat.sabacc.infra.ws.utils.SabaccPubSub
import io.lemniscat.sabacc.infra.ws.utils.getQueryString
import org.springframework.stereotype.Component
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.WebSocketSession
import reactor.core.publisher.*

@Component
class SabaccWebSocketHandler(
        val sabaccPubSub: SabaccPubSub
): WebSocketHandler {
    override fun handle(session: WebSocketSession): Mono<Void> {
        var idString = session.handshakeInfo.uri.getQueryString()["id"]!! // TODO: handle the !!, plus we should not trust user input
        var input = session.receive().doOnNext { message ->
                    message
                }.concatMap { message ->
                    sabaccPubSub.accept(WebSocketRequest(message, session.id, Id.of(idString)))
                    Mono.just(message)
                }.then()
        var output = session.send(
                sabaccPubSub.fluxProcessor
                .filter {
                    println("msg $it")
                    it.sessionId == session.id
                }.map {
                    val r = ObjectMapper().writeValueAsString(it)
                            println(r)
                            r
                }.map(session::textMessage))
        return Mono.zip(input, output).then()
    }
}

enum class GameFlowStatus {
    InGame,
    OutOfGame,
    WaitingForSuddenDemise,
    WaitingForReveal
}

class SabaccUsers {
    private val idToSession: MutableMap<Id, String> = mutableMapOf()
    private val sessionToId: MutableMap<String, Id> = mutableMapOf()
    val idToFlowStatus: MutableMap<Id, GameFlowStatus> = mutableMapOf()

    fun register(userId: Id, session: String) {
        idToSession.put(userId, session)
        sessionToId.put(session, userId)
        idToFlowStatus.putIfAbsent(userId, GameFlowStatus.InGame)
    }

    fun getSession(userId: Id) = UserSession(idToSession[userId]!!, userId)
    fun getSession(sessionId: String) = UserSession(sessionId, sessionToId[sessionId]!!)

    fun outOfGame(userId: Id) = idToFlowStatus.set(userId, GameFlowStatus.OutOfGame)
    fun waitForSuddenDemise(userId: Id) = idToFlowStatus.set(userId, GameFlowStatus.WaitingForSuddenDemise)
    fun waitForReveal(userId: Id) = idToFlowStatus.set(userId, GameFlowStatus.WaitingForReveal)

    fun areAllWaitingForSuddenDemise(players: List<Player>) = players.all {
        val status = idToFlowStatus.get(it.id)
        status != null && (status == GameFlowStatus.OutOfGame || status == GameFlowStatus.WaitingForSuddenDemise)
    }

    fun areAllWaitingForReveal(players: List<Player>) = players.all {
        val status = idToFlowStatus.get(it.id)
        status != null && (status == GameFlowStatus.OutOfGame || status == GameFlowStatus.WaitingForReveal)
    }
}
