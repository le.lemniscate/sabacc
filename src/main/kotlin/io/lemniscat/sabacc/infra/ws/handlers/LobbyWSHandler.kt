package io.lemniscat.sabacc.infra.ws.handlers

import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.lemniscat.sabacc.domain.repositories.LobbyRepository
import io.lemniscat.sabacc.domain.repositories.UserRepository
import io.lemniscat.sabacc.domain.usecases.game.StartGame
import io.lemniscat.sabacc.domain.usecases.lobby.Create
import io.lemniscat.sabacc.domain.usecases.lobby.Join
import io.lemniscat.sabacc.domain.usecases.lobby.Leave
import org.springframework.stereotype.Component
import kotlin.random.Random

@Component
class LobbyWSHandler(
        private val userRepository: UserRepository,
        private val lobbyRepository: LobbyRepository,
        private val gamingTableRepository: GamingTableRepository
) {
    fun create(request: InputMessage<CreateLobby>): OutputMessage =
            Create(lobbyRepository, userRepository)(
                    request.requester.userId,
                    request.action.allowance
            ).map {
                OutputMessage.of(LobbyMessageTypes.LOBBY_CREATED, it, request.requester.userId)
            }.getOrElseGet {
                OutputMessage.of(it, request.requester.userId)
            }

    fun join(request: InputMessage<JoinLobby>): OutputMessage =
            Join(lobbyRepository, userRepository)(
                    request.requester.userId,
                    Id.of(request.action.lobbyId)
            ).map {
                OutputMessage.of(LobbyMessageTypes.LOBBY_JOINED, it, request.requester.userId)
            }.getOrElseGet {
                OutputMessage.of(it, request.requester.userId)
            }

    fun leave(request: InputMessage<LeaveLobby>): OutputMessage =
            Leave(lobbyRepository, userRepository)(
                    request.requester.userId,
                    Id.of(request.action.lobbyId)
            ).map {
                OutputMessage.of(LobbyMessageTypes.LOBBY_LEFT, it, request.requester.userId)
            }.getOrElseGet {
                OutputMessage.of(it, request.requester.userId)
            }

    fun launch(request: InputMessage<LaunchGame>): OutputMessage =
            (lobbyRepository retrieve Id.of(request.action.lobbyId)).map { lobby ->
                StartGame(gamingTableRepository, Random(Math.random().toInt()))(
                        lobby
                )
            }.map { result ->
                if (result.isRight) {
                    OutputMessage.of(GameMessageTypes.GAME_LAUNCHED, result.get(), request.requester.userId)
                } else {
                    OutputMessage.of(result.left, request.requester.userId)
                }
            }.getOrElseGet {
                OutputMessage.of(it, request.requester.userId)
            }

}