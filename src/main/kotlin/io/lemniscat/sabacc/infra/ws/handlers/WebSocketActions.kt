package io.lemniscat.sabacc.infra.ws.handlers

interface WebSocketAction

interface GameAction: WebSocketAction {
    val gameId: String
}

interface CardAction: GameAction {
    val suit: String
    val name: String
    val value: Int
    val type: String
}

data class CreateLobby(
        val userId: String,
        val allowance: Int
): WebSocketAction

data class JoinLobby(
        val userId: String,
        val lobbyId: String
): WebSocketAction

data class LeaveLobby(
        val userId: String,
        val lobbyId: String
): WebSocketAction

data class LaunchGame(
        val userId: String,
        val lobbyId: String
): WebSocketAction

data class GetGame(
        override val gameId: String
): GameAction

data class CallHand(
        override val gameId: String
): GameAction

data class DrawCard(
        override val gameId: String
): GameAction

data class PlayerFold(
        override val gameId: String
): GameAction

data class PlayerMatchBet(
        override val gameId: String
): GameAction

data class PlayerRaiseBet(
        override val gameId: String,
        val amount: Int
): GameAction

data class PlayerStand(
        override val gameId: String
): GameAction

data class ReadyForSuddenDemise(
        override val gameId: String
): GameAction

data class ReadyForReveal(
        override val gameId: String
): GameAction

data class PutCardInInterferenceField(
        override val gameId: String,
        override val suit: String,
        override val name: String,
        override val value: Int,
        override val type: String
): CardAction

data class TradeCard(
        override val gameId: String,
        override val suit: String,
        override val name: String,
        override val value: Int,
        override val type: String
): CardAction

data class RemoveCardFromInterferenceField(
        override val gameId: String,
        override val suit: String,
        override val name: String,
        override val value: Int,
        override val type: String
): CardAction
