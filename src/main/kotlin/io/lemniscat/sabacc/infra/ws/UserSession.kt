package io.lemniscat.sabacc.infra.ws

import io.lemniscat.sabacc.domain.model.misc.Id

data class UserSession(
        val sessionId: String,
        val userId: Id
)