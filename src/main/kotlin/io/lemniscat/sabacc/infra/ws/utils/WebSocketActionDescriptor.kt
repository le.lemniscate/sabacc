package io.lemniscat.sabacc.infra.ws.utils

import io.lemniscat.sabacc.infra.ws.handlers.WebSocketAction
import io.lemniscat.sabacc.infra.ws.handlers.OutputMessage
import io.lemniscat.sabacc.infra.ws.handlers.InputMessage

data class WebSocketActionDescriptor(
        val actionClass: Class<out WebSocketAction>,
        val handler: (InputMessage<WebSocketAction>) -> OutputMessage
)
