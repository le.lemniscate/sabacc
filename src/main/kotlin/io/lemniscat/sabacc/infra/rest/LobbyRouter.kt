package io.lemniscat.sabacc.infra.rest

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.router

@Configuration
class LobbyRouter(val lobbyRestHandler: LobbyRestHandler) {
    @Bean
    fun lobbyRoutes() = router {
        GET("/lobby", lobbyRestHandler::getAll)
        POST("/register", lobbyRestHandler::register)
        POST("/login", lobbyRestHandler::login)
        POST("/lobby", lobbyRestHandler::create)
        PUT("/lobby", lobbyRestHandler::join)
        PATCH("/lobby", lobbyRestHandler::launch)
        DELETE("/lobby", lobbyRestHandler::leave)
    }
}