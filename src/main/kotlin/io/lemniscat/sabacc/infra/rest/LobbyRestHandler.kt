package io.lemniscat.sabacc.infra.rest

import io.lemniscat.sabacc.domain.error.LobbyError
import io.lemniscat.sabacc.domain.model.User
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.player.PlayerName
import io.lemniscat.sabacc.domain.usecases.game.StartGame
import io.lemniscat.sabacc.domain.usecases.lobby.Create
import io.lemniscat.sabacc.domain.usecases.lobby.GetAll
import io.lemniscat.sabacc.domain.usecases.lobby.Join
import io.lemniscat.sabacc.domain.usecases.lobby.Leave
import io.lemniscat.sabacc.infra.dto.Error
import io.lemniscat.sabacc.infra.dto.GamingTablePlayerProjection
import io.lemniscat.sabacc.infra.dto.LobbyDTO
import io.lemniscat.sabacc.infra.dto.UserDTO
import io.lemniscat.sabacc.infra.repositories.GamingTableMemoryRepository
import io.lemniscat.sabacc.infra.repositories.LobbyMemoryRepository
import io.lemniscat.sabacc.infra.repositories.UserMemoryRepository
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyToMono
import reactor.core.publisher.Mono
import kotlin.random.Random

data class LobbyCreationPayload(
    val userId: String,
    val allowance: Int
)

data class LobbyPayload(
    val userId: String,
    val lobbyId: String
)

data class LoginPayload(
    val username: String
)

fun error(it: LobbyError) =
        ServerResponse.badRequest()
                .body(Mono.just(Error.from(it)), Error::class.java)

@Component
class LobbyRestHandler(
        private val userMemoryRepository: UserMemoryRepository,
        private val lobbyMemoryRepository: LobbyMemoryRepository,
        private val gamingTableMemoryRepository: GamingTableMemoryRepository
) {
    fun getAll(request: ServerRequest): Mono<out ServerResponse> =
                        send(GetAll(lobbyMemoryRepository)().map(LobbyDTO.Factories::from))

    fun create(request: ServerRequest): Mono<out ServerResponse> =
            request.bodyToMono<LobbyCreationPayload>()
                    .flatMap {
                        Create(lobbyMemoryRepository, userMemoryRepository)(
                                Id.of(it.userId),
                                it.allowance
                        ).map {
                            send(LobbyDTO.from(it))
                        }.getOrElseGet {
                            error(it)
                        }
                    }

    fun join(request: ServerRequest): Mono<out ServerResponse> =
            request.bodyToMono<LobbyPayload>()
                    .flatMap {
                        Join(lobbyMemoryRepository, userMemoryRepository)(
                                Id.of(it.userId),
                                Id.of(it.lobbyId)
                        ).map {
                            send(LobbyDTO.from(it))
                        }.getOrElseGet {
                            error(it)
                        }
                    }

    fun leave(request: ServerRequest): Mono<out ServerResponse> =
            request.bodyToMono<LobbyPayload>()
                    .flatMap {
                        Leave(lobbyMemoryRepository, userMemoryRepository)(
                                Id.of(it.userId),
                                Id.of(it.lobbyId)
                        ).map {
                            send(LobbyDTO.from(it))
                        }.getOrElseGet {
                            error(it)
                        }
                    }

    fun launch(request: ServerRequest): Mono<out ServerResponse> =
            request.bodyToMono<LobbyPayload>()
                    .flatMap {
                        (lobbyMemoryRepository retrieve Id.of(it.lobbyId)).map { lobby ->
                            StartGame(gamingTableMemoryRepository, Random(Math.random().toInt()))(
                                    lobby
                            )
                        }.map { result ->
                            if (result.isRight) {
                                send(GamingTablePlayerProjection.project(result.get(), Id.of(it.userId)))
                            } else {
                                error(result.left)
                            }
                        }.getOrElseGet {
                            error(it)
                        }
                    }

    fun login(request: ServerRequest): Mono<out ServerResponse> =
            request.bodyToMono<LoginPayload>()
                    .flatMap {
                        (userMemoryRepository retrieve PlayerName.of(it.username)).map {
                            send(UserDTO.from(it))
                        }.getOrElseGet {
                            error(it)
                        }
                    }

    fun register(request: ServerRequest): Mono<out ServerResponse> =
            request.bodyToMono<LoginPayload>()
                    .flatMap {
                        (userMemoryRepository save User.of(PlayerName.of(it.username))).map {
                            send(UserDTO.from(it))
                        }.getOrElseGet {
                            error(it)
                        }
                    }

}