package io.lemniscat.sabacc.infra.rest

import io.lemniscat.sabacc.domain.error.GameError
import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.card.*
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult
import io.lemniscat.sabacc.domain.usecases.game.*
import io.lemniscat.sabacc.infra.dto.Error
import io.lemniscat.sabacc.infra.dto.FACE
import io.lemniscat.sabacc.infra.dto.GamingTablePlayerProjection
import io.lemniscat.sabacc.infra.dto.SUIT
import io.lemniscat.sabacc.infra.errors.InvalidRequestError
import io.lemniscat.sabacc.infra.repositories.GamingTableMemoryRepository
import io.vavr.control.Either
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyToMono
import reactor.core.publisher.Mono

data class CardPayload(
        val type: String,
        val suit: String,
        val value: String
)

fun Card.Factories.from(payload: CardPayload): Either<GameError, Card> =
        when (payload.type) {
            SUIT -> {
                try {
                    Either.right<GameError, Card>(SuitCard.of(StandardSuit.valueOf(payload.suit), SuitValue.valueOf(payload.value)))
                } catch (e: IllegalArgumentException) {
                    Either.left<GameError, Card>(InvalidRequestError())
                }
            }
            FACE -> {
                try {
                    Either.right<GameError, Card>(FaceCard.of(FaceSuit.valueOf(payload.suit), FaceValue.valueOf(payload.value)))
                } catch (e: IllegalArgumentException) {
                    Either.left<GameError, Card>(InvalidRequestError())
                }
            }
            else -> Either.left(InvalidRequestError())
        }

interface GameActionPayload {
    val gameId: String
    val playerId: String
}

data class BasicPayload(
    override val gameId: String,
    override val playerId: String
): GameActionPayload

data class PayloadWithCard(
    override val gameId: String,
    override val playerId: String,
    val card: CardPayload
): GameActionPayload

data class PayloadWithCredits(
        override val gameId: String,
        override val playerId: String,
        val credits: Int
): GameActionPayload

fun error(it: GameError) =
        ServerResponse.badRequest()
                .body(Mono.just(Error.from(it)), Error::class.java)

typealias UseCaseCall<T> = (gamingTable: GamingTable, playerId: Id, payload: T) -> UseCaseResult

inline fun <reified T: GameActionPayload> playerAction(
        request: ServerRequest,
        gamingTableMemoryRepository: GamingTableMemoryRepository,
        crossinline usecase: UseCaseCall<T>
): Mono<out ServerResponse> =
    request.bodyToMono<T>()
        .flatMap {
            val gameId = Id.of(it.gameId)
            val playerId = Id.of(it.playerId)
            (gamingTableMemoryRepository retrieve gameId)
                .flatMap { game ->
                    usecase(game, playerId, it)
                }.map {
                    send(GamingTablePlayerProjection.project(it, playerId))
                }.getOrElseGet {
                    error(it)
                }
        }

@Component
class SabaccRestHandler(
        private val gamingTableMemoryRepository: GamingTableMemoryRepository
) {
    fun call(request: ServerRequest): Mono<out ServerResponse> = playerAction<BasicPayload>(request, gamingTableMemoryRepository) { gamingTable, playerId, _ ->
        Call(gamingTableMemoryRepository)(
                gamingTable,
                playerId
        )
    }

    fun draw(request: ServerRequest) = playerAction<BasicPayload>(request, gamingTableMemoryRepository) { gamingTable, playerId, _ ->
        Draw(gamingTableMemoryRepository)(
                gamingTable,
                playerId
        )
    }

    fun fold(request: ServerRequest) = playerAction<BasicPayload>(request, gamingTableMemoryRepository) { gamingTable, playerId, _ ->
        Fold(gamingTableMemoryRepository)(
                gamingTable,
                playerId
        )
    }

    fun matchBet(request: ServerRequest) = playerAction<BasicPayload>(request, gamingTableMemoryRepository) { gamingTable, playerId, _ ->
        MatchBet(gamingTableMemoryRepository)(
                gamingTable,
                playerId
        )
    }

    fun putInInterferenceField(request: ServerRequest) = playerAction<PayloadWithCard>(request, gamingTableMemoryRepository) { gamingTable, playerId, payload ->
        Card.from(payload.card)
                .flatMap {
                    PutInInterferenceField(gamingTableMemoryRepository)(
                            gamingTable,
                            playerId,
                            it
                    )
                }
    }

    fun removeFromInterferenceField(request: ServerRequest) = playerAction<PayloadWithCard>(request, gamingTableMemoryRepository) { gamingTable, playerId, payload ->
        Card.from(payload.card)
                .flatMap {
                    RemoveFromInterferenceField(gamingTableMemoryRepository)(
                            gamingTable,
                            playerId,
                            it
                    )
                }
    }

    fun stand(request: ServerRequest) = playerAction<BasicPayload>(request, gamingTableMemoryRepository) { gamingTable, playerId, _ ->
        Stand(gamingTableMemoryRepository)(
                gamingTable,
                playerId
        )
    }

    fun trade(request: ServerRequest) = playerAction<PayloadWithCard>(request, gamingTableMemoryRepository) { gamingTable, playerId, payload ->
        Card.from(payload.card)
                .flatMap {
                    RemoveFromInterferenceField(gamingTableMemoryRepository)(
                            gamingTable,
                            playerId,
                            it
                    )
                }
    }

    fun raise(request: ServerRequest) = playerAction<PayloadWithCredits>(request, gamingTableMemoryRepository) { gamingTable, playerId, payload ->
        RaiseBet(gamingTableMemoryRepository)(
                gamingTable,
                playerId,
                payload.credits
        )
    }

}
