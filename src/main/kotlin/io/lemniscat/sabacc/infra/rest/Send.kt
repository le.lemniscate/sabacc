package io.lemniscat.sabacc.infra.rest

import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

inline fun <reified T> send(value: T) =
        ServerResponse.ok()
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(value), T::class.java)