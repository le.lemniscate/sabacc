package io.lemniscat.sabacc.infra.rest

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.EntityResponse
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

@Configuration
class SabaccRouter(val sabaccRestHandler: SabaccRestHandler) {
    @Bean
    fun sabaccRoutes() = router {
        POST("/call", sabaccRestHandler::call)
        POST("/draw", sabaccRestHandler::draw)
        POST("/fold", sabaccRestHandler::fold)
        POST("/match", sabaccRestHandler::matchBet)
        PUT("/interference", sabaccRestHandler::putInInterferenceField)
        DELETE("/interference", sabaccRestHandler::removeFromInterferenceField)
        POST("/stand", sabaccRestHandler::stand)
        POST("/trade", sabaccRestHandler::trade)
        POST("/raise", sabaccRestHandler::raise)
    }
}