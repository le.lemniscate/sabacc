package io.lemniscat.sabacc.infra.repositories

import io.lemniscat.sabacc.domain.error.GamingTableNotFound
import io.lemniscat.sabacc.domain.error.GamingTableNotSaved
import io.lemniscat.sabacc.domain.helpers.generators.IdGenerator
import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.vavr.control.Either
import org.springframework.stereotype.Component

@Component
class GamingTableMemoryRepository(val generator: IdGenerator): GamingTableRepository {
    val content = mutableMapOf<Id, GamingTable>()

    override fun save(gamingTable: GamingTable): UseCaseResult =
        when {
            gamingTable.id.isEmpty() -> {
                val id = generator()
                content[id] = gamingTable.copy(id = id)
                Either.right(content[id])
            }
            content[gamingTable.id] == null -> {
                Either.left(GamingTableNotSaved())
            }
            else -> {
                content[gamingTable.id] = gamingTable
                Either.right(content[gamingTable.id])
            }
        }

    override fun retrieve(id: Id): UseCaseResult =
        when {
            content[id] == null -> {
                Either.left(GamingTableNotFound())
            }
            else -> Either.right(content[id])
        }
}