package io.lemniscat.sabacc.infra.repositories

import io.lemniscat.sabacc.domain.error.*
import io.lemniscat.sabacc.domain.helpers.generators.IdGenerator
import io.lemniscat.sabacc.domain.model.Lobby
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.misc.LobbyResult
import io.lemniscat.sabacc.domain.repositories.LobbyRepository
import io.vavr.control.Either
import org.springframework.stereotype.Component

@Component
class LobbyMemoryRepository(val generator: IdGenerator): LobbyRepository {
    val content = mutableMapOf<Id, Lobby>()

    override fun save(lobby: Lobby): LobbyResult =
        when {
            lobby.id.isEmpty() -> {
                val id = generator()
                content[id] = lobby.copy(id = id)
                Either.right(content[id])
            }
            content[lobby.id] == null -> {
                Either.left(LobbyNotSaved())
            }
            else -> {
                content[lobby.id] = lobby
                Either.right(content[lobby.id])
            }
        }

    override fun retrieve(id: Id): LobbyResult =
        when {
            content[id] == null -> {
                Either.left(LobbyNotFound())
            }
            else -> Either.right(content[id])
        }

    override fun retrieveAll(): List<Lobby> = content.values.toList()
}