package io.lemniscat.sabacc.infra.repositories

import io.lemniscat.sabacc.domain.helpers.generators.IdGenerator
import io.lemniscat.sabacc.domain.model.misc.Id
import org.springframework.stereotype.Component
import java.util.*

@Component
class UUIDGenerator: IdGenerator {
    override fun invoke(): Id =
        Id.of(UUID.randomUUID().toString())
}
