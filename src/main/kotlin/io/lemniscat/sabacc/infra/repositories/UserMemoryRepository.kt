package io.lemniscat.sabacc.infra.repositories

import io.lemniscat.sabacc.domain.error.LobbyNotFound
import io.lemniscat.sabacc.domain.error.LobbyNotSaved
import io.lemniscat.sabacc.domain.error.UserNotFound
import io.lemniscat.sabacc.domain.helpers.generators.IdGenerator
import io.lemniscat.sabacc.domain.model.Lobby
import io.lemniscat.sabacc.domain.model.User
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.misc.UserResult
import io.lemniscat.sabacc.domain.model.player.PlayerName
import io.lemniscat.sabacc.domain.repositories.UserRepository
import io.vavr.control.Either
import org.springframework.stereotype.Component

@Component
class UserMemoryRepository(val generator: IdGenerator): UserRepository {
    val content = mutableMapOf<Id, User>()

    override fun save(user: User): UserResult =
            when {
                user.id.isEmpty() -> {
                    val id = generator()
                    content[id] = user.copy(id = id)
                    Either.right(content[id])
                }
                content[user.id] == null -> {
                    Either.left(UserNotFound())
                }
                else -> {
                    content[user.id] = user
                    Either.right(content[user.id])
                }
            }

    override fun retrieve(id: Id): UserResult =
            when {
                content[id] == null -> {
                    Either.left(UserNotFound())
                }
                else -> Either.right(content[id])
            }

    override fun retrieve(name: PlayerName): UserResult {
        val value = content.values.find { it.name == name }
        return if (value != null) {
            Either.right(value)
        } else {
            Either.left(UserNotFound())
        }
    }
}