package io.lemniscat.sabacc.domain.model.player

import io.lemniscat.sabacc.domain.error.GameError
import io.lemniscat.sabacc.domain.error.NotEnoughCredits
import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.Pot
import io.lemniscat.sabacc.domain.model.User
import io.lemniscat.sabacc.domain.model.card.Cards
import io.lemniscat.sabacc.domain.model.scoring.*
import io.vavr.control.Either

data class Player(
        val id: Id,
        val name: PlayerName,
        val credits: Int,
        val status: PlayerStatus,
        val hand: Hand,
        val interferenceField: InterferenceField
) {
    companion object Factories {
        fun empty(): Player = Player(
                Id.empty(),
                PlayerName.empty(),
                0,
                PlayerStatus.inGame(),
                Hand.empty(),
                InterferenceField.empty()
        )

        fun from(user: User, allowance: Int): Player = Player(
                user.id,
                user.name,
                allowance,
                PlayerStatus.inGame(),
                Hand.empty(),
                InterferenceField.empty()
        )
    }

    val stillInGame: Boolean
        get() = !(status is OutOfMoney || status is Fold || status is BombedOut)
    val handSize: Int
        get() = hand.cards.size

    fun ante() =
            when {
                !stillInGame -> this
                credits > 1 -> copy(credits = credits - 2)
                else -> copy(status = OutOfMoney())
            }

    fun fold() = copy(status = PlayerStatus.fold())

    fun spend(amount: Int): Either<GameError, Player> =
            if (credits >= amount) {
                Either.right(copy(credits = credits - amount))
            } else {
                Either.left(NotEnoughCredits())
            }

    fun owns(card: Card): Boolean =
            hand.cards.contains(card)

    fun interfered(card: Card): Boolean =
            interferenceField.cards.contains(card)

    fun interfer(card: Card) = copy(
            hand = hand - card,
            interferenceField = interferenceField + card
    )
    fun takeBack(card: Card) = copy(
            hand = hand + card,
            interferenceField = interferenceField - card
    )

    fun declareWinner(vararg pots: Pot) = pots.fold(copy(status = PlayerStatus.won())) { player, pot ->
        println("Player win ${player.win(pot).credits}")
        player.win(pot)
    }

    fun win(amount: Int): Player = copy(credits = credits + amount)
    fun win(pot: Pot): Player = win(pot.credits)
    fun deal(vararg cards: Card) = deal(cards.toList())
    fun deal(cards: List<Card>) = copy(hand = hand + cards)
    fun discard(vararg cards: Card) = discard(cards.toList())
    fun discard(cards: List<Card>) = copy(hand = hand - cards)

    fun getScore(): Score {
        val cards = Cards.from(hand, interferenceField)
        val total = cards.total()
        return when {
            cards.containsExactlyValues(0, 2, 3) -> IdiotsArray.of(this, 23)
            total == 23 -> PureSabacc.of(this, 23)
            total == -23 -> NegativePureSabacc.of(this, -23)
            total in 1 until 23 -> PositiveScore.of(this, total)
            total in -22 until 0 -> NegativeScore.of(this, total)
            else -> Bomb(this, total)
        }
    }
}

