package io.lemniscat.sabacc.domain.model.card

interface Card {
    companion object Factories

    val value: Value
    val suit: Suit
}

