package io.lemniscat.sabacc.domain.model.card

interface Value {
    val name: String
    val points: Int
}
