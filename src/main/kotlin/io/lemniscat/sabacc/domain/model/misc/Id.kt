package io.lemniscat.sabacc.domain.model.misc

interface Id {
    companion object Factories {
        fun empty(): Id = EmptyId()
        fun of(value: String): Id = ConcreteId(value)
    }

    val value: String

    fun isEmpty(): Boolean
}

