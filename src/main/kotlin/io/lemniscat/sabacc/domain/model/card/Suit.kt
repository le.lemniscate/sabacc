package io.lemniscat.sabacc.domain.model.card

interface Suit {
    val name: String
}

enum class StandardSuit: Suit {
    Sabers, Flasks, Coins, Staves
}

