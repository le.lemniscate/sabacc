package io.lemniscat.sabacc.domain.model

data class Pot(val credits: Int) {
    companion object Factories {
        fun empty() = Pot(0)
    }

    operator fun plus(amount: Int): Pot = copy(credits = this.credits + amount)
    operator fun plus(pot: Pot): Pot = copy(credits = this.credits + pot.credits)
}