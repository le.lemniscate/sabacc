package io.lemniscat.sabacc.domain.model.player

import io.lemniscat.sabacc.domain.model.card.Card

data class InterferenceField(val cards: List<Card>) {
    companion object Factories {
        fun of(vararg cards: Card) = InterferenceField(cards.asList())
        fun of(cards: List<Card>) = InterferenceField(cards)
        fun empty(): InterferenceField = InterferenceField(listOf())
    }

    operator fun plus(card: Card) = copy(cards = cards + card)
    operator fun minus(card: Card) = copy(cards = cards - card)
}