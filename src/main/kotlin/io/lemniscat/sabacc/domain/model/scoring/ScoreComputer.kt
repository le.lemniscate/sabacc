package io.lemniscat.sabacc.domain.model.scoring

import io.lemniscat.sabacc.domain.model.player.PlayerStatus
import io.lemniscat.sabacc.domain.model.player.Players

interface GameResult

fun whoBombedOut(scores: List<Score>): Players =
        Players.of(scores.fold(mutableListOf()) { acc, score ->
            if (score is Bomb) {
                acc.add(score.player.copy(status = PlayerStatus.bombedOut()))
            }
            acc
        })

fun electWinner(scores: List<Score>): GameResult =
    when {
        scores.isEmpty() || scores[0] is Bomb -> AllBombedOut.between(Players.of(scores.map { it.player }))
        scores[0] > scores[1] -> Winner.elect(scores[0])
        else -> {
            var i = 0
            do {
                i++
            } while (i < scores.size && !(scores[i - 1] > scores[i]))
            Tie(Players.of(scores.subList(0, i).map { it.player }))
        }
    }
