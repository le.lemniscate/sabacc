package io.lemniscat.sabacc.domain.model

import io.lemniscat.sabacc.domain.model.misc.Id

data class Lobby(
        val id: Id,
        val users: List<User>,
        val allowance: Int
) {
    companion object Factories {
        fun empty() = Lobby(Id.empty(), listOf(), 0)
        fun of(users: List<User>) = Lobby(Id.empty(), users, 0)
    }

    infix fun joinedBy(user: User): Lobby =
        if (users.contains(user)) {
            this
        } else {
            copy(users = users + user)
        }

    infix fun leftBy(user: User) = copy(users = users - user)

    fun fixAllowance(value: Int) = copy(allowance = value)

    val numberOfPlayers: Int
        get() = users.size
}