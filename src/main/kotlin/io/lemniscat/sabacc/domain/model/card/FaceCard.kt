package io.lemniscat.sabacc.domain.model.card

data class FaceCard(
        override val suit: FaceSuit,
        override val value: FaceValue
): Card {
    companion object Factories {
        fun of(suit: FaceSuit, value: FaceValue) = FaceCard(suit, value)
    }
}