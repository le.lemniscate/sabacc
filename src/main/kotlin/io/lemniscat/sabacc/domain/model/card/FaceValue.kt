package io.lemniscat.sabacc.domain.model.card

enum class FaceValue(override val points: Int): Value {
    STAR(-17),
    EVIL_ONE(-15),
    MODERATION(-14),
    DEMISE(-13),
    BALANCE(-11),
    ENDURANCE(-8),
    QUEEN_OF_AIR_AND_DARKNESS(-2),
    IDIOT(0)
}