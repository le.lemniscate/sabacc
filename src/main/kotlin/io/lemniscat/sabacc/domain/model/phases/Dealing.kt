package io.lemniscat.sabacc.domain.model.phases

import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.player.Player
import io.lemniscat.sabacc.domain.model.player.Players

data class Dealing(
        override val startingPlayer: Id,
        override val currentPlayer: Id
): TurnPhase {
    override val name = PhasesNames.Dealing

    companion object Factories {
        fun start(players: Players) = start(players, Id.empty())

        fun start(players: Players, startingPlayer: Id): Dealing {
            var eligiblePlayer = players[startingPlayer]
                .map {
                    if (it.stillInGame) {
                        it
                    } else {
                        players.getPlayerAfter(startingPlayer)
                    }
                }.getOrElseGet {
                    players.first()
                }
            return Dealing(eligiblePlayer.id, eligiblePlayer.id)
        }
    }

    override fun moveToPlayer(id: Id): TurnPhase = copy(currentPlayer = id)
    override fun moveToPlayer(player: Player): TurnPhase = moveToPlayer(player.id)
}