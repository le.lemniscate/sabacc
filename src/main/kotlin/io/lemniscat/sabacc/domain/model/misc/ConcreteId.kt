package io.lemniscat.sabacc.domain.model.misc

data class ConcreteId(override val value: String): Id {
    override fun isEmpty(): Boolean = false
}