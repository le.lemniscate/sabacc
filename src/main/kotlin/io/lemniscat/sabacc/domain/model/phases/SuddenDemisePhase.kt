package io.lemniscat.sabacc.domain.model.phases

import io.lemniscat.sabacc.domain.model.misc.Id

data class SuddenDemisePhase(
        val playersInvolved: List<Id>
): GamePhase {
    override val name = PhasesNames.SuddenDemise

    companion object Factories {
        fun start(involved: List<Id>) = SuddenDemisePhase(
                involved
        )

        fun start(vararg ids: Id) = SuddenDemisePhase(
                ids.toList()
        )
    }
}