package io.lemniscat.sabacc.domain.model.phases

import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.player.Player

interface TurnPhase: GamePhase {
    val currentPlayer: Id
    val startingPlayer: Id

    fun moveToPlayer(id: Id): TurnPhase
    fun moveToPlayer(player: Player): TurnPhase
}