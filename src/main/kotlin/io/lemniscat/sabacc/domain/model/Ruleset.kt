package io.lemniscat.sabacc.domain.model

data class Ruleset(
        val numberOfDealtCards: Int,
        val potBuildingLength: Int,
        val rulesetName: String
) {
    companion object Factories {
        fun basic() = Ruleset(2, 4, "basic")
        fun extended() = Ruleset(5, 4, "extended")
    }

    fun isPotBuildingPhaseOver(round: Int) = round >= potBuildingLength
    fun render(): String = rulesetName
}