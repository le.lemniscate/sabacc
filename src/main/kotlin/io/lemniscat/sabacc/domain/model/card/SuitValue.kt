package io.lemniscat.sabacc.domain.model.card

enum class SuitValue(override val points: Int): Value {
    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    ELEVEN(11),
    COMMANDER(12),
    MISTRESS(13),
    MASTER(14),
    ACE(15)
}

const val MAX_SUIT_VALUE = 15
