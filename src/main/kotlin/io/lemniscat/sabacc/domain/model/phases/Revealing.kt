package io.lemniscat.sabacc.domain.model.phases

class Revealing: GamePhase {
    override val name = PhasesNames.Revealing

    companion object Factories {
        fun start() = Revealing()
    }
}