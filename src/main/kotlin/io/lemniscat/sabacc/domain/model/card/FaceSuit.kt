package io.lemniscat.sabacc.domain.model.card

enum class FaceSuit: Suit {
    Black, White
}