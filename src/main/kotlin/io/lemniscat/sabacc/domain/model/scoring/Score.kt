package io.lemniscat.sabacc.domain.model.scoring

import io.lemniscat.sabacc.domain.model.player.Player
import kotlin.math.abs

const val loose = -1
val win = 1
val tie = 0

interface Score: Comparable<Score> {
    val player: Player
    val value: Int

    override fun compareTo(other: Score): Int
}

class IdiotsArray(override val player: Player, override val value: Int): Score {
    companion object Factories {
        fun of(player: Player, value: Int) = IdiotsArray(player, value)
    }

    override fun compareTo(other: Score): Int =
            when (other) {
                is IdiotsArray -> tie
                else -> win
            }
}

class PureSabacc(override val player: Player, override val value: Int): Score {
    companion object Factories {
        fun of(player: Player, value: Int) = PureSabacc(player, value)
    }

    override fun compareTo(other: Score): Int =
            when (other) {
                is IdiotsArray -> loose
                is PureSabacc -> tie
                else -> win
            }
}

class NegativePureSabacc(override val player: Player, override val value: Int): Score {
    companion object Factories {
        fun of(player: Player, value: Int) = NegativePureSabacc(player, value)
    }

    override fun compareTo(other: Score): Int =
            when (other) {
                is IdiotsArray -> loose
                is PureSabacc -> loose
                is NegativePureSabacc -> tie
                else -> win
            }
}

class PositiveScore(override val player: Player, override val value: Int): Score {
    companion object Factories {
        fun of(player: Player, value: Int) = PositiveScore(player, value)
    }

    override fun compareTo(other: Score): Int =
            when (other) {
                is IdiotsArray -> loose
                is PureSabacc -> loose
                is NegativePureSabacc -> loose
                is NegativeScore -> if (abs(other.value) > value) {
                    loose
                } else {
                    win
                }
                is PositiveScore -> value - other.value
                else -> win
            }
}

class NegativeScore(override val player: Player, override val value: Int): Score {
    companion object Factories {
        fun of(player: Player, value: Int) = NegativeScore(player, value)
    }

    override fun compareTo(other: Score): Int =
            when (other) {
                is IdiotsArray -> loose
                is PureSabacc -> loose
                is NegativePureSabacc -> loose
                is NegativeScore -> abs(value) - abs(other.value)
                is PositiveScore -> if (abs(value) > other.value) {
                    win
                } else {
                    loose
                }
                else -> win
            }
}

class Bomb(override val player: Player, override val value: Int): Score {
    companion object Factories {
        fun of(player: Player, value: Int) = Bomb(player, value)
    }

    override fun compareTo(other: Score): Int =
            when (other) {
                is Bomb -> tie
                else -> loose
            }
}
