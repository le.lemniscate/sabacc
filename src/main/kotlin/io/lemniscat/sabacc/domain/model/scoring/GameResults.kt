package io.lemniscat.sabacc.domain.model.scoring

import io.lemniscat.sabacc.domain.model.player.Players

data class Tie(val between: Players): GameResult {
    companion object Factories {
        fun between(players: Players) = Tie(players)
    }
}

data class Winner(val score: Score): GameResult {
    companion object Factories {
        fun elect(score: Score) = Winner(score)
    }
}

data class AllBombedOut(val players: Players): GameResult {
    companion object Factories {
        fun between(players: Players) = AllBombedOut(players)
    }
}
