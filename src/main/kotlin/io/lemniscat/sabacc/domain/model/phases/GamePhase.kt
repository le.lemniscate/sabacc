package io.lemniscat.sabacc.domain.model.phases

interface GamePhase {
    companion object Factories
    val name: PhasesNames
}
