package io.lemniscat.sabacc.domain.model.player

interface PlayerStatus {
    companion object Factories {
        fun inGame(): PlayerStatus = InGame()
        fun fold(): PlayerStatus = Fold()
        fun outOfMoney(): PlayerStatus = OutOfMoney()
        fun bombedOut(): PlayerStatus = BombedOut()
        fun won(): PlayerStatus = Won()
    }

    fun render(): String
}

class InGame: PlayerStatus {
    override fun render(): String = "InGame"
}

class OutOfMoney: PlayerStatus {
    override fun render(): String = "OutOfMoney"
}

class Fold: PlayerStatus {
    override fun render(): String = "Fold"
}

class BombedOut: PlayerStatus {
    override fun render(): String = "BombedOut"
}

class Won: PlayerStatus {
    override fun render(): String = "Won"
}
