package io.lemniscat.sabacc.domain.model.card

import io.lemniscat.sabacc.domain.error.GameError
import io.lemniscat.sabacc.domain.error.NotEnoughCardsToDeal
import io.lemniscat.sabacc.domain.model.player.Hand
import io.vavr.control.Either
import kotlin.random.Random


data class Deck(val cards: List<Card>) {

    data class DrawResult(val deck: Deck, val cards: List<Card>)

    companion object Factories {
        fun of(cards: List<Card>) = Deck(cards)

        fun of(vararg cards: Card) = Deck(cards.asList())

        fun shuffle(randomizer: Random = Random(Math.random().toInt())): Deck {
            val cards = mutableListOf(
                    StandardSuit.values().flatMap { suit ->
                        SuitValue.values().fold(mutableListOf<Card>()) { acc, value ->
                            acc.add(SuitCard.of(suit, value))
                            acc
                        }
                    },
                    FaceSuit.values().flatMap { suit ->
                        FaceValue.values().map { value ->
                            FaceCard.of(suit, value)
                        }
                    }
            ).flatten()
            return of(cards.shuffled(randomizer))
        }
    }

    val size: Int
        get() = cards.size

    operator fun plus(hand: Hand) = copy(cards = cards + hand.cards)

    fun draw(numberOfCards: Int): Either<GameError, DrawResult> =
        if (numberOfCards <= cards.size) {
            val drawn = cards.subList(0, numberOfCards)
            Either.right(DrawResult(copy(cards = this.cards - drawn), drawn))
        } else {
            Either.left(NotEnoughCardsToDeal())
        }

    fun addAtBottom(vararg toAdd: Card): Deck =
            copy(cards = cards + toAdd)

    fun shuffle(randomizer: Random) = copy(cards = cards.shuffled(randomizer))
}
