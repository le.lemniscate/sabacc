package io.lemniscat.sabacc.domain.model

import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.player.PlayerName

data class User(
        val id: Id,
        val name: PlayerName
) {
    companion object Factories {
        fun of(name: PlayerName) = User(Id.empty(), name)
    }
}