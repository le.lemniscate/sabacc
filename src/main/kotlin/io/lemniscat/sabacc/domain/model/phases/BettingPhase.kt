package io.lemniscat.sabacc.domain.model.phases

import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.player.Player

interface BettingPhase: TurnPhase {
    val currentBet: Int

    fun raisedBy(player: Player): BettingPhase
    fun raisedBy(id: Id): BettingPhase
    fun raisedTo(amount: Int): BettingPhase
}
