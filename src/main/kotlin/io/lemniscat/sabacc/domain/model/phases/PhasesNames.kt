package io.lemniscat.sabacc.domain.model.phases

enum class PhasesNames {
    GameStarted,
    Betting,
    Dealing,
    Calling,
    Revealing,
    SuddenDemise,
    GameOver
}