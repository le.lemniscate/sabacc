package io.lemniscat.sabacc.domain.model.player

data class PlayerName(val value: String) {
    companion object Factories {
        fun empty(): PlayerName = PlayerName("")
        fun of(value: String): PlayerName = PlayerName(value)
    }
}