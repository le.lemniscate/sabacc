package io.lemniscat.sabacc.domain.model

import io.lemniscat.sabacc.domain.error.*
import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.card.Deck
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult
import io.lemniscat.sabacc.domain.model.phases.*
import io.lemniscat.sabacc.domain.model.player.Hand
import io.lemniscat.sabacc.domain.model.player.Player
import io.lemniscat.sabacc.domain.model.player.PlayerStatus
import io.lemniscat.sabacc.domain.model.player.Players
import io.lemniscat.sabacc.domain.model.scoring.*
import io.vavr.control.Either
import kotlin.random.Random

data class GamingTable(
        val id: Id,
        val players: Players,
        val round: Int,
        val phase: GamePhase,
        val mainPot: Pot,
        val sabaccPot: Pot,
        val ruleset: Ruleset,
        val deck: Deck
) {
    companion object Factories {
        fun empty() = GamingTable(
                Id.empty(),
                Players.empty(),
                0,
                GameStarted.start(),
                Pot.empty(),
                Pot.empty(),
                Ruleset.basic(),
                Deck.shuffle(Random(Math.random().toInt()))
        )

        fun from(players: Players): Either<GameError, GamingTable> =
                empty().withPlayers(players)

        fun from(lobby: Lobby) = from(
                Players.of(lobby.users, lobby.allowance)
        )
    }

    val numberOfActivePlayers: Int
        get() = players.numberStillInGame

    val firstPlayer: Player
        get() = players.first()

    fun hasOnlyOnePlayerRemaining() = numberOfActivePlayers == 1
    fun moveTo(phase: GamePhase): GamingTable = copy(phase = phase)
    fun increaseRound(): GamingTable = copy(round = round + 1)

    fun get(id: Id): Player = players.content.first { it.id == id }

    fun replacePlayer(player: Player): GamingTable =
            copy(players = players.replace(player))

    fun increaseMainPot(amount: Int) = copy(mainPot = mainPot + amount)

    fun withPlayers(players: Players): Either<GameError, GamingTable> =
            when {
                players.numberStillInGame < 2 -> Either.left(NotEnoughPlayers())
                players.numberStillInGame > 8 -> Either.left(TooManyPlayers())
                else ->
                    Either.right(copy(players = players))
            }

    fun distributeCards(): UseCaseResult =
            deck.draw(numberOfActivePlayers * ruleset.numberOfDealtCards)
                    .flatMap { drawn ->
                        players.distribute(drawn.cards).map {
                            copy(
                                    players = it,
                                    deck = drawn.deck
                            )
                        }
                    }

    infix fun dealTo(playerId: Id): UseCaseResult =
            deck.draw(1)
                    .map { drawn ->
                        copy(
                                players = players.replace(get(playerId).deal(drawn.cards)),
                                deck = drawn.deck
                        )
                    }

    infix fun dealTo(players: List<Id>): UseCaseResult =
            players.fold(Either.right(this)) { either, id ->
                if (either.isLeft) {
                    either
                } else {
                    val game = either.get()
                    game dealTo id
                }
            }

    infix fun shift(random: Random): UseCaseResult {
        val deck = players
                .takeBackAllCard(deck)
                .shuffle(random)
        val result: Either<GameError, GamingTable> = Either.right(copy(deck = deck))
        return players.content.fold(result) { either, player ->
            either.flatMap {
                it replaceCardsOf player
            }
        }
    }

    private infix fun replaceCardsOf(player: Player): UseCaseResult =
        deck.draw(player.handSize)
            .map { drawn ->
                copy(
                    players = players.replace(player.copy(hand = Hand.of(drawn.cards))),
                    deck = drawn.deck
                )
            }

    infix fun shuffleDeck(random: Random): GamingTable =
        copy(deck = Deck.shuffle(random))

    fun tradeCard(playerId: Id, card: Card): UseCaseResult {
        val player = get(playerId)
        return if (player.owns(card)) {
            deck.draw(1)
                .map { drawn ->
                    copy(
                        players = players.replace(player.deal(drawn.cards).discard(card)),
                        deck = drawn.deck.addAtBottom(card)
                    )
                }
        } else {
            Either.left(CardNotOwned())
        }
    }

    infix fun bombOut(bombedOutPlayers: Players): GamingTable {
        val mainPotCredits = mainPot.credits
        var contributionToPot = 0
        var toModify = bombedOutPlayers.map { player ->
            player.spend(mainPotCredits)
                .map {
                    contributionToPot += mainPotCredits
                    it
                }
                .getOrElseGet {
                    contributionToPot += player.credits
                    player.copy(
                        credits = 0,
                        status = PlayerStatus.outOfMoney()
                    )
                }
        }
        return copy(
            players = players.replaceAll(toModify),
            sabaccPot = sabaccPot.plus(contributionToPot)
        )
    }

    infix fun bombOutWithNoPenalty(bombedOut: Players): GamingTable =
        copy(
            players = players.replaceAll(bombedOut)
        )

    fun declareAllLoosers(): GamingTable =
        copy(
            mainPot = Pot.empty(),
            sabaccPot = sabaccPot + mainPot,
            phase = GameOver.start(Id.empty())
        )

    infix fun declare(tie: Tie): GamingTable =
            copy(
                    phase = SuddenDemisePhase.start(tie.between.ids())
            )

    infix fun declare(winner: Winner): GamingTable =
            when (winner.score) {
                is PureSabacc, is NegativePureSabacc, is IdiotsArray -> declarePureSabaccWinner(winner.score.player)
                else -> declareWinner(winner.score.player)
            }

    infix fun declareWinner(player: Player): GamingTable =
            copy(
                    players = players.replace(player.declareWinner(mainPot)),
                    mainPot = Pot.empty(),
                    phase = GameOver(player.id)
            )

    fun lastStandingPlayerWins(): UseCaseResult =
            if (hasOnlyOnePlayerRemaining()) {
                Either.right(declareWinner(firstPlayer))
            } else {
                Either.left(MultiplePlayerLeft())
            }

    infix fun declarePureSabaccWinner(player: Player): GamingTable =
        copy(
                players = players.replace(player.declareWinner(mainPot, sabaccPot)),
                mainPot = Pot.empty(),
                sabaccPot = Pot.empty(),
                phase = GameOver(player.id)
        )

    fun moveToInterferenceField(playerId: Id, card: Card): UseCaseResult {
        val player = get(playerId)
        return if (player.owns(card)) {
            Either.right(copy(
                    players = players.replace(player.interfer(card))
            ))
        } else {
            Either.left(CardNotOwned())
        }
    }

    fun takeFromInterferenceField(playerId: Id, card: Card): UseCaseResult {
        val player = get(playerId)
        return if (player.interfered(card)) {
            Either.right(copy(
                    players = players.replace(player.takeBack(card))
            ))
        } else {
            Either.left(CardNotInInterferenceField())
        }
    }

    inline fun <reified T> checkThatPhaseIs(): UseCaseResult =
            if (phase is T) {
                Either.right(this)
            } else {
                Either.left(InvalidStateForCalledAction())
            }

    infix fun checkIfPlayerIsStillInGame(playerId: Id): UseCaseResult =
            if (get(playerId).stillInGame) {
                Either.right(this)
            } else {
                Either.left(NotPlayerTurn())
            }

    infix fun checkIfCurrentPlayerIs(playerId: Id): UseCaseResult =
            if (phase is TurnPhase && phase.currentPlayer == playerId) {
                Either.right(this)
            } else {
                Either.left(NotPlayerTurn())
            }

    infix fun isCalledBy(playerId: Id): GamingTable =
            copy(phase = Calling.start(players, playerId))

    infix fun movesToPlayerAfter(playerId: Id): UseCaseResult {
        val phase = phase
        return if (phase is TurnPhase) {
            val nextPlayer = players.getPlayerAfter(playerId)
            if (nextPlayer.id == phase.startingPlayer) {
                nextPhase(phase.startingPlayer)
            } else {
                Either.right(
                        copy(phase = phase.moveToPlayer(nextPlayer))
                )
            }
        } else {
            Either.left(InvalidStateForCalledAction())
        }
    }

    fun nextPhase(startingPlayer: Id): UseCaseResult {
        return when (phase) {
            is GameStarted -> Either.right(
                    moveTo(Betting.start(Players.active(players), startingPlayer)))
            is Betting -> Either.right(
                    moveTo(Dealing.start(Players.active(players), startingPlayer)).increaseRound())
            is Dealing -> Either.right(
                    moveTo(Betting.start(Players.active(players), startingPlayer)))
            is Calling -> Either.right(
                    moveTo(Revealing.start()))
            else -> Either.left(
                    InvalidStateForGoingToNextPhase())
        }
    }

    fun isPotBuildingPhaseOver(): UseCaseResult =
        if (ruleset.isPotBuildingPhaseOver(round)) {
            Either.right(this)
        } else {
            Either.left(PotBuildingNotOver())
        }

    fun ante(): UseCaseResult {
        val playersAfterAnte = players.map { it.ante() }
        return withPlayers(playersAfterAnte)
                .map {
                    it.copy(
                            mainPot = it.mainPot + playersAfterAnte.numberStillInGame,
                            sabaccPot = it.sabaccPot + playersAfterAnte.numberStillInGame
                    )
                }
    }

    infix fun checkRaiseAmount(amount: Int): UseCaseResult =
            if (amount <= (phase as BettingPhase).currentBet) {
                Either.left(BetTooLow())
            } else {
                Either.right(this)
            }

    fun hasNewRaiser(playerId: Id, amount: Int): GamingTable =
            copy(phase = (phase as BettingPhase).raisedBy(playerId).raisedTo(amount))

    fun spend(playerId: Id, currentBet: Int) =
            get(playerId).spend(currentBet).map {
                replacePlayer(it).increaseMainPot(currentBet)
            }

    infix fun fold(playerId: Id): GamingTable =
            replacePlayer(get(playerId).fold())
}
