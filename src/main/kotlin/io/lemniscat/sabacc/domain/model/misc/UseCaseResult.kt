package io.lemniscat.sabacc.domain.model.misc

import io.lemniscat.sabacc.domain.error.GameError
import io.lemniscat.sabacc.domain.error.LobbyError
import io.lemniscat.sabacc.domain.error.NotEnoughCardsToDeal
import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.Lobby
import io.lemniscat.sabacc.domain.model.User
import io.vavr.control.Either

typealias UseCaseResult = Either<GameError, GamingTable>
typealias LobbyResult = Either<LobbyError, Lobby>
typealias UserResult = Either<LobbyError, User>

fun UseCaseResult.wasNotEnoughCardsToDeal() = this.isLeft && this.left is NotEnoughCardsToDeal
