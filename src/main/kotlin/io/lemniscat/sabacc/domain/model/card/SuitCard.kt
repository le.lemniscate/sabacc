package io.lemniscat.sabacc.domain.model.card

data class SuitCard(
        override val suit: Suit,
        override val value: SuitValue
): Card {
    companion object Factories {
        fun of(suit: Suit, value: SuitValue) = SuitCard(suit, value)
    }
}