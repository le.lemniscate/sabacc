package io.lemniscat.sabacc.domain.model.phases

class GameStarted: GamePhase {
    override val name = PhasesNames.GameStarted

    companion object Factories {
        fun start() = GameStarted()
    }
}