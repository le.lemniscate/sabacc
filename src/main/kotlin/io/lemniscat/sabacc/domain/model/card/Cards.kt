package io.lemniscat.sabacc.domain.model.card

import io.lemniscat.sabacc.domain.model.player.Hand
import io.lemniscat.sabacc.domain.model.player.InterferenceField

data class Cards(val content: List<Card>) {
    companion object Factories {
        fun from(hand: Hand, interferenceField: InterferenceField) =
                Cards(hand.cards + interferenceField.cards)
    }

    fun total() = content.map { it.value.points }.sum()

    fun containsExactlyValues(vararg values: Int) =
            content.size == values.size
                    && content.map { it.value.points }.containsAll(values.toList())
}
