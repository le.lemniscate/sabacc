package io.lemniscat.sabacc.domain.model.phases

import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.player.Player

data class GameOver(val winner: Id): GamePhase {
    override val name = PhasesNames.GameOver

    companion object Factories {
        fun start(winner: Player) = start(winner.id)
        fun start(winner: Id) = GameOver(winner)
    }
}