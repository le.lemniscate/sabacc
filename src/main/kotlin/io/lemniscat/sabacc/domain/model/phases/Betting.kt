package io.lemniscat.sabacc.domain.model.phases

import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.player.Player
import io.lemniscat.sabacc.domain.model.player.Players

data class Betting(
        override val currentPlayer: Id,
        override val currentBet: Int,
        override val startingPlayer: Id
): BettingPhase {
    override val name = PhasesNames.Betting

    companion object Factories {
        fun start(players: Players) = start(players, Id.empty())

        fun start(players: Players, startingPlayer: Id): Betting {
            var eligiblePlayer = when {
                startingPlayer.isEmpty() -> players.first()
                else -> players[startingPlayer]
                        .map {
                            if (it.stillInGame) {
                                it
                            } else {
                                players.getPlayerAfter(startingPlayer)
                            }
                        }.getOrElseGet {
                            players.first()
                        }
            }
            return Betting(
                    eligiblePlayer.id,
                    1,
                    eligiblePlayer.id
            )
        }
    }

    override fun raisedBy(player: Player): BettingPhase = copy(startingPlayer = player.id)
    override fun raisedBy(id: Id): BettingPhase = copy(startingPlayer = id)
    override fun raisedTo(amount: Int): BettingPhase = copy(currentBet = amount)

    override fun moveToPlayer(id: Id): TurnPhase = copy(currentPlayer = id)
    override fun moveToPlayer(player: Player): TurnPhase = moveToPlayer(player.id)
}