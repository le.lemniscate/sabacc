package io.lemniscat.sabacc.domain.model.player

import io.lemniscat.sabacc.domain.error.GameError
import io.lemniscat.sabacc.domain.error.NotEnoughCardsToDeal
import io.lemniscat.sabacc.domain.error.PlayerNotFound
import io.lemniscat.sabacc.domain.model.User
import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.card.Deck
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.phases.SuddenDemisePhase
import io.lemniscat.sabacc.domain.model.scoring.Score
import io.vavr.control.Either

data class Players(val content: List<Player>) {
    companion object Factories {
        fun empty() = Players(listOf())

        fun of(content: List<Player>) = Players(content)

        fun of(content: List<User>, allowance: Int) = Players(content.map {
            Player.from(it, allowance)
        })

        fun active(players: Players) = of(players.content.filter { it.stillInGame })
    }

    operator fun get(id: Id): Either<GameError, Player> {
        val result = content.find { it.id == id }
        return if (result != null) {
            Either.right(result)
        } else {
            Either.left(PlayerNotFound())
        }
    }

    val number: Int
        get() = content.size
    val numberStillInGame: Int
        get() = content.count { it.stillInGame }
    private val scoreList: List<Score>
        get() = content.map { it.getScore() }

    fun first() = content.first { it.stillInGame }

    infix fun involvedIn(phase: SuddenDemisePhase) =
            filter { player -> player.id in phase.playersInvolved }

    // TODO: remove following lines
    private fun filter(callback: (player: Player) -> Boolean): Players = of(content = content.filter(callback))
    fun map(callback: (player: Player) -> Player): Players = copy(content = content.map(callback))
    private fun <T> map(callback: (player: Player) -> T): List<T> = content.map(callback)
    fun ids(): List<Id> = content.map { it.id }

    fun replace(player: Player): Players = map {
        if (it.id == player.id) {
            player
        } else {
            it
        }
    }

    fun replaceAll(players: Players): Players = map { tested ->
        players.content.find { tested.id == it.id } ?: tested
    }

    private fun stillInGame(): Players = of(content.fold(listOf()) { acc, player ->
        if (player.stillInGame) {
            acc + player
        } else {
            acc
        }
    })

    fun forEach(callback: (player: Player) -> Unit): Unit = content.forEach(callback)

    fun scores(): List<Score> =
            stillInGame().scoreList.sorted().reversed()

    fun getPlayerAfter(playerId: Id): Player {
        var playerPosition = content.indexOfFirst { it.id == playerId }
        var nextPlayer: Player
        do {
            playerPosition = if (playerPosition == content.size - 1) {
                0
            } else {
                playerPosition + 1
            }
            nextPlayer = content[playerPosition]
        } while (!nextPlayer.stillInGame)
        return nextPlayer
    }

    fun distribute(cards: List<Card>): Either<GameError, Players> {
        return if (cards.size % numberStillInGame == 0) {
            val cardsPerPlayer = cards.size / numberStillInGame
            var index = 0
            Either.right(copy(content = content.map { player ->
                if (player.stillInGame) {
                    val firstCard = index * cardsPerPlayer
                    index += 1
                    player.deal(cards.subList(firstCard, firstCard + cardsPerPlayer))
                } else {
                    player
                }
            }))
        } else {
            Either.left(NotEnoughCardsToDeal())
        }
    }

    fun takeBackAllCard(deck: Deck) = content.fold(deck) {
        acc, player -> acc + player.hand
    }

    operator fun plus(player: Player) = copy(content = content + player)
}