package io.lemniscat.sabacc.domain.model.misc

data class EmptyId(override val value: String = ""): Id {
    override fun isEmpty(): Boolean = true
}