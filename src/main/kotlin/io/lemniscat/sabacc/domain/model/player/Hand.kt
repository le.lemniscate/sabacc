package io.lemniscat.sabacc.domain.model.player

import io.lemniscat.sabacc.domain.model.card.Card

data class Hand(val cards: List<Card>) {
    companion object Factories {
        fun empty() = Hand(listOf())
        fun of(cards: List<Card>) = Hand(cards)
        fun of(vararg cards: Card) = of(cards.toList())
    }

    operator fun plus(newCards: List<Card>) = copy(cards = cards + newCards)
    operator fun plus(card: Card) = copy(cards = cards + card)
    operator fun minus(toRemove: List<Card>) = copy(cards = cards - toRemove)
    operator fun minus(toRemove: Card) = copy(cards = cards - toRemove)

    fun contains(card: Card) = cards.contains(card)

}
