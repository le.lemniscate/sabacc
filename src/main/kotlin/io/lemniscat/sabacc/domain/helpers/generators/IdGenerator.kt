package io.lemniscat.sabacc.domain.helpers.generators

import io.lemniscat.sabacc.domain.model.misc.Id
import java.util.*

interface IdGenerator {
    operator fun invoke(): Id
}

class UUIDGenerator: IdGenerator {
    override operator fun invoke(): Id {
        return Id.of(UUID.randomUUID().toString())
    }
}
