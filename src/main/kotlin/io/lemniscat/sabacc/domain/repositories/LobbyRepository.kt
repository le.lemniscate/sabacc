package io.lemniscat.sabacc.domain.repositories

import io.lemniscat.sabacc.domain.model.Lobby
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.misc.LobbyResult

interface LobbyRepository {
    infix fun save(lobby: Lobby): LobbyResult
    infix fun retrieve(id: Id): LobbyResult
    fun retrieveAll(): List<Lobby>
}