package io.lemniscat.sabacc.domain.repositories

import io.lemniscat.sabacc.domain.model.User
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.misc.UserResult
import io.lemniscat.sabacc.domain.model.player.PlayerName

interface UserRepository {
    infix fun save(player: User): UserResult
    infix fun retrieve(id: Id): UserResult
    infix fun retrieve(name: PlayerName): UserResult
}