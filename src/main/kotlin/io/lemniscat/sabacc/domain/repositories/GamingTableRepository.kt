package io.lemniscat.sabacc.domain.repositories

import io.lemniscat.sabacc.domain.error.GameError
import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult
import io.vavr.control.Either

interface GamingTableRepository {
    infix fun save(gamingTable: GamingTable): UseCaseResult
    infix fun retrieve(id: Id): UseCaseResult
}