package io.lemniscat.sabacc.domain.error

class InvalidStateForCalledAction: GameError
class InvalidStateForGoingToNextPhase: GameError
class NotPlayerTurn: GameError
class PotBuildingNotOver: GameError
class MultiplePlayerLeft: GameError
