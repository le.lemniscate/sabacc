package io.lemniscat.sabacc.domain.error

class NotEnoughPlayers: GameError
class TooManyPlayers: GameError
class PlayerNotFound: GameError
