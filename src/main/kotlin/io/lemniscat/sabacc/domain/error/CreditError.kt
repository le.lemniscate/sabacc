package io.lemniscat.sabacc.domain.error

class NotEnoughCredits: GameError
class BetTooLow: GameError
