package io.lemniscat.sabacc.domain.error

class NotEnoughCardsToDeal: GameError
class CardNotOwned: GameError
class CardNotInInterferenceField: GameError
