package io.lemniscat.sabacc.domain.error

interface LobbyError

class LobbyNotFound: LobbyError
class LobbyNotSaved: LobbyError
class UserNotFound: LobbyError
class UserNotSaved: LobbyError
