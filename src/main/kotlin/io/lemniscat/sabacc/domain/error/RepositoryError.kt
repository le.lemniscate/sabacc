package io.lemniscat.sabacc.domain.error

class GamingTableNotFound: GameError
class GamingTableNotSaved: GameError
