package io.lemniscat.sabacc.domain.usecases.lobby

import io.lemniscat.sabacc.domain.model.Lobby
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.misc.LobbyResult
import io.lemniscat.sabacc.domain.repositories.LobbyRepository
import io.lemniscat.sabacc.domain.repositories.UserRepository

class Create(
        private val repository: LobbyRepository,
        private val userRepository: UserRepository
) {
    operator fun invoke(userId: Id, allowance: Int): LobbyResult =
        (userRepository retrieve userId).map {
            Lobby.of(listOf(it))
                    .fixAllowance(allowance)
        }.flatMap {
            repository save it
        }
}