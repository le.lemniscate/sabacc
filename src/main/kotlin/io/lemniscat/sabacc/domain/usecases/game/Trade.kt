package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.phases.Dealing
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult

class Trade(
        private val repository: GamingTableRepository
) {
    operator fun invoke(gamingTable: GamingTable, playerId: Id, card: Card): UseCaseResult =
        gamingTable.checkThatPhaseIs<Dealing>()
            .flatMap {
                it checkIfCurrentPlayerIs playerId
            }.flatMap {
                it.tradeCard(playerId, card)
            }.flatMap {
                it movesToPlayerAfter playerId
            }.flatMap {
                repository save it
            }
}
