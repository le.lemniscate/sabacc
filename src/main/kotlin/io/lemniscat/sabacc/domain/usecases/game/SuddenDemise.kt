package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.phases.SuddenDemisePhase
import io.lemniscat.sabacc.domain.model.scoring.*
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult
import io.lemniscat.sabacc.domain.model.misc.wasNotEnoughCardsToDeal
import io.vavr.control.Either

class SuddenDemise(
        private val repository: GamingTableRepository
) {
    operator fun invoke(gamingTable: GamingTable): UseCaseResult =
        gamingTable.checkThatPhaseIs<SuddenDemisePhase>()
            .flatMap {
                it.dealTo((gamingTable.phase as SuddenDemisePhase).playersInvolved)
            }.let {
                when {
                    it.isRight -> {
                        val oldTable = it.get()
                        val phase = oldTable.phase as SuddenDemisePhase
                        val scores = oldTable.players.involvedIn(phase).scores()
                        val game = oldTable bombOutWithNoPenalty whoBombedOut(scores)
                        Either.right(when (val result = electWinner(scores)) {
                            is Winner -> game declare result
                            is Tie -> game declare result
                            else -> { // All bombed out!
                                val globalWinner = electWinner(game.players.scores())
                                if (globalWinner is Winner) {
                                    game declare globalWinner
                                } else {
                                    game.declareAllLoosers()
                                }
                            }
                        })
                    }
                    it.wasNotEnoughCardsToDeal() -> Either.right(gamingTable.declareAllLoosers())
                    else -> it
                }
            }.flatMap {
                repository save it
            }
}