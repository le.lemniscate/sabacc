package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.phases.TurnPhase
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult

class RemoveFromInterferenceField(
        private val repository: GamingTableRepository
) {
    operator fun invoke(gamingTable: GamingTable, playerId: Id, card: Card): UseCaseResult =
        gamingTable.checkThatPhaseIs<TurnPhase>()
            .flatMap {
                it.takeFromInterferenceField(playerId, card)
            }.flatMap {
                repository save it
            }
}
