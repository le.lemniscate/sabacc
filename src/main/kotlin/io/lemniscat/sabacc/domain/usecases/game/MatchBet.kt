package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.phases.BettingPhase
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult

class MatchBet(
        private val repository: GamingTableRepository
) {
    operator fun invoke(gamingTable: GamingTable, playerId: Id): UseCaseResult =
        gamingTable.checkThatPhaseIs<BettingPhase>()
                .flatMap {
                    it checkIfCurrentPlayerIs playerId
                }.flatMap {
                    it.spend(playerId, (gamingTable.phase as BettingPhase).currentBet)
                }.flatMap {
                    it movesToPlayerAfter playerId
                }.flatMap {
                    repository save it
                }
}