package io.lemniscat.sabacc.domain.usecases.lobby

import io.lemniscat.sabacc.domain.model.Lobby
import io.lemniscat.sabacc.domain.repositories.LobbyRepository

class GetAll(
        private val repository: LobbyRepository
) {
    operator fun invoke(): List<Lobby> = repository.retrieveAll()
}