package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.error.GameError
import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.phases.TurnPhase
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.vavr.control.Either

class PutInInterferenceField(
        private val repository: GamingTableRepository
) {
    operator fun invoke(gamingTable: GamingTable, playerId: Id, card: Card): Either<GameError, GamingTable> =
            gamingTable.checkThatPhaseIs<TurnPhase>()
                    .flatMap {
                        it.moveToInterferenceField(playerId, card)
                    }.flatMap {
                        repository save it
                    }
}
