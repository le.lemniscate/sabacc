package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.phases.Betting
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult

class Fold(
        private val repository: GamingTableRepository
) {
    operator fun invoke(gamingTable: GamingTable, playerId: Id): UseCaseResult =
        gamingTable.checkThatPhaseIs<Betting>()
            .flatMap {
                it checkIfPlayerIsStillInGame playerId
            }.flatMap {
                it checkIfCurrentPlayerIs playerId
            }.map {
                it fold playerId
            }.flatMap {
                when {
                    it.hasOnlyOnePlayerRemaining() -> it.lastStandingPlayerWins()
                    else -> it movesToPlayerAfter playerId
                }
            }.flatMap {
                repository save it
            }
    }
