package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.phases.Dealing
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult

class Draw(
        private val repository: GamingTableRepository
) {
    operator fun invoke(gamingTable: GamingTable, playerId: Id): UseCaseResult =
        gamingTable.checkThatPhaseIs<Dealing>()
            .flatMap {
                it checkIfCurrentPlayerIs playerId
            }.flatMap {
                it dealTo playerId
            }.flatMap {
                it movesToPlayerAfter playerId
            }.flatMap {
                repository save it
            }
}
