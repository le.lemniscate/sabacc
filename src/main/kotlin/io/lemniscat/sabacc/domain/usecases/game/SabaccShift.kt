package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.phases.TurnPhase
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult
import kotlin.random.Random

class SabaccShift(
        private val repository: GamingTableRepository
) {
    operator fun invoke(gamingTable: GamingTable, random: Random = Random(Math.random().toInt())): UseCaseResult =
            gamingTable.checkThatPhaseIs<TurnPhase>()
                    .flatMap {
                        it shift random
                    }.flatMap {
                        repository save it
                    }
}
