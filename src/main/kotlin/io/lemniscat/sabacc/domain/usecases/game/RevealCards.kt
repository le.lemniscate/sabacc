package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.phases.Revealing
import io.lemniscat.sabacc.domain.model.scoring.*
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult
import io.vavr.control.Either

class RevealCards(
        private val repository: GamingTableRepository
) {
    operator fun invoke(gamingTable: GamingTable): UseCaseResult =
        gamingTable.checkThatPhaseIs<Revealing>()
                .flatMap {
                    when {
                        gamingTable.hasOnlyOnePlayerRemaining() -> it.lastStandingPlayerWins()
                        else -> {
                            var scores = gamingTable.players.scores()
                            var game = it bombOut whoBombedOut(scores)
                            Either.right(when (val finalScore = electWinner(scores)) {
                                is Winner -> game declare finalScore
                                is Tie -> game declare finalScore
                                else -> game.declareAllLoosers()
                            })
                        }
                    }
                }.flatMap {
                    repository save it
                }
}