package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.phases.Betting
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult
import io.lemniscat.sabacc.domain.model.phases.TurnPhase

class Call(
        private val repository: GamingTableRepository
) {
    operator fun invoke(gamingTable: GamingTable, playerId: Id): UseCaseResult =
        gamingTable.checkThatPhaseIs<Betting>()
            .flatMap {
                it.isPotBuildingPhaseOver()
            }.flatMap {
                it checkIfCurrentPlayerIs playerId
            }.map {
                it isCalledBy playerId
            }.flatMap {
                repository save it
            }
}
