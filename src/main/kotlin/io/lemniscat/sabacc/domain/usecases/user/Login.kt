package io.lemniscat.sabacc.domain.usecases.user

import io.lemniscat.sabacc.domain.model.User
import io.lemniscat.sabacc.domain.model.misc.UserResult
import io.lemniscat.sabacc.domain.model.player.PlayerName
import io.lemniscat.sabacc.domain.repositories.UserRepository

class Login(
        private val repository: UserRepository
) {
    operator fun invoke(userName: PlayerName): UserResult =
            (repository retrieve userName).map {
                it
            }.orElse {
                repository save User.of(userName)
            }
}