package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.error.GameError
import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.Lobby
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.vavr.control.Either
import kotlin.random.Random

// TODO: remove Lobby from the lobby repo
class StartGame(
        private val repository: GamingTableRepository,
        private val random: Random
) {
    operator fun invoke(lobby: Lobby): Either<GameError, GamingTable> {
        return GamingTable.from(lobby).flatMap {
            it.ante()
        }.map {
            it shuffleDeck random
        }.flatMap {
            it.distributeCards()
        }.flatMap {
            it.nextPhase(Id.empty())
        }.flatMap {
            repository save it
        }
    }
}