package io.lemniscat.sabacc.domain.usecases.lobby

import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.misc.LobbyResult
import io.lemniscat.sabacc.domain.repositories.LobbyRepository
import io.lemniscat.sabacc.domain.repositories.UserRepository

class Leave(
        private val repository: LobbyRepository,
        private val userRepository: UserRepository
) {
    operator fun invoke(userId: Id, lobbyId: Id): LobbyResult =
            (userRepository retrieve userId).flatMap {
                (repository retrieve lobbyId).map { lobby ->
                    lobby leftBy it
                }.flatMap {
                    repository save it
                }
            }
}