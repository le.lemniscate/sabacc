package io.lemniscat.sabacc.domain.helpers.generators

import kotlin.random.Random

interface IntGenerator {
    val n: Int
    operator fun invoke(): Int
}

class AtMost(maxValue: Int): IntGenerator {
    override val n = Random.nextInt(maxValue)
    override fun invoke() = n
}

class AtLeast(minValue: Int): IntGenerator {
    override val n = Random.nextInt(minValue, 10000)
    override operator fun invoke(): Int = n
}

class Between(minValue: Int, maxValue: Int): IntGenerator {
    override val n = Random.nextInt(minValue, maxValue)
    override operator fun invoke(): Int = n
}

class PositiveOrNegativeBetween(minValue: Int, maxValue: Int): IntGenerator {
    override val n = Random.nextInt(minValue, maxValue) * if (Random.nextBoolean()) {
        -1
    } else {
        1
    }
    override operator fun invoke(): Int = n
}

class Fixed(override val n: Int): IntGenerator {
    override fun invoke() = n
}

val correctAmountOfPlayers = Between(2, 8)

val anyHandSize = Between(1, 76)

val littleHand = Between(1, 76 / 8) // To ensure we always have enough cards in the deck
