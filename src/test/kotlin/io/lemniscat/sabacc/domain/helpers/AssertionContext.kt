package io.lemniscat.sabacc.domain.helpers

import io.lemniscat.sabacc.domain.error.*
import io.lemniscat.sabacc.domain.helpers.generators.IntGenerator
import io.lemniscat.sabacc.domain.model.Pot
import io.lemniscat.sabacc.domain.model.RANDOMIZED_CARDS
import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.phases.*
import io.lemniscat.sabacc.domain.model.player.BombedOut
import io.lemniscat.sabacc.domain.model.player.OutOfMoney
import io.lemniscat.sabacc.domain.model.player.Player
import io.lemniscat.sabacc.domain.helpers.accessors.IntAccessor
import io.lemniscat.sabacc.domain.helpers.generators.GameGenerator
import io.lemniscat.sabacc.domain.helpers.generators.LobbyGenerator
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import org.assertj.core.api.Assertions.*

class AssertionContext(
        val gamingTableRepository: GamingTableRepository,
        val gameGenerator: GameGenerator,
        val lobbyGenerator: LobbyGenerator,
        val performingContext: PerformingContext
) {

    fun phaseIs(value: String) {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            when (value) {
                "dealing" -> assertThat(it.phase).isInstanceOf(Dealing::class.java)
                "calling" -> assertThat(it.phase).isInstanceOf(Calling::class.java)
                "betting" -> assertThat(it.phase).isInstanceOf(Betting::class.java)
                "revealing" -> assertThat(it.phase).isInstanceOf(Revealing::class.java)
                "game over" -> assertThat(it.phase).isInstanceOf(GameOver::class.java)
                "sudden demise" -> assertThat(it.phase).isInstanceOf(SuddenDemisePhase::class.java)
                else -> throw AssertionError("Game phase unknown: $value")
            }
        }
    }

    fun winnerIs(pointer: String): PlayerAsserter {
        assertThat(performingContext.result!!.isRight).isTrue()
        return performingContext.result!!.map {
            val player = gameGenerator.retrievePlayer(pointer)
            assertThat((it.phase as GameOver).winner).isEqualTo(player)
            PlayerAsserter(this, it.get(player))
        }.get()
    }

    fun noWinner() {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            assertThat((it.phase as GameOver).winner.isEmpty()).isTrue()
        }
    }

    fun players(): PlayersAsserter {
        assertThat(performingContext.result!!.isRight).isTrue()
        return performingContext.result!!.map {
            lobbyGenerator.players.values
            PlayersAsserter(this, lobbyGenerator.players.keys.map { id -> it.get(id) })
        }.get()
    }

    fun player(pointer: String): PlayerAsserter {
        assertThat(performingContext.result!!.isRight).isTrue()
        return performingContext.result!!.map {
            val player = gameGenerator.retrievePlayer(pointer)
            PlayerAsserter(this, it.get(player))
        }.get()
    }

    fun mainPotContains(value: Int) {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            assertThat(it.mainPot.credits).isEqualTo(value)
        }
    }

    // TODO: remove this one
    fun mainPotGained(value: String) {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            assertThat(it.mainPot.credits).isEqualTo(
                gameGenerator.getCredits(value) + performingContext.gameGenerator().mainPot.credits
            )
        }
    }

    fun numberOfPlayersIs(vararg accessors: IntAccessor) {
        assertThat(performingContext.result!!.isRight).isTrue()
        val targetNumber = accessors.fold(0) {
            accumulator, accessor -> accumulator + accessor()
        }
        performingContext.result!!.map {
            assertThat(it.players.numberStillInGame).isEqualTo(targetNumber)
        }
    }

    fun mainPotIs(vararg accessors: IntAccessor) {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            potIs(it.mainPot, *accessors)
        }
    }

    fun sabaccPotIs(vararg accessors: IntAccessor) {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            potIs(it.sabaccPot, *accessors)
        }
    }

    private fun potIs(pot: Pot, vararg accessors: IntAccessor) {
        val targetValue = accessors.fold(0) { accumulator, accessor -> accumulator + accessor() }
        assertThat(pot.credits).isEqualTo(
                targetValue
        )
    }

    fun mainPotGained(vararg accessors: IntAccessor) {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            potGained(it.mainPot, performingContext.gameGenerator().mainPot, *accessors)
        }
    }

    fun sabaccPotGained(vararg accessors: IntAccessor) {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            println("Main pot original: ${performingContext.gameGenerator().mainPot}")
            println("Sabacc pot original: ${performingContext.gameGenerator().sabaccPot}")
            println("Sabacc pot result: ${it.sabaccPot}")
            potGained(it.sabaccPot, performingContext.gameGenerator().sabaccPot, *accessors)
        }
    }

    private fun potGained(pot: Pot, basePot: Pot, vararg accessors: IntAccessor) {
        val targetValue = accessors.fold(0) { accumulator, accessor -> accumulator + accessor() }
        assertThat(pot.credits).isEqualTo(
                targetValue + basePot.credits
        )
    }

    fun playerIs(pointer: String) {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            assertThat((it.phase as TurnPhase).currentPlayer).isEqualTo(gameGenerator.retrievePlayer(pointer))
        }
    }

    fun callerIs(pointer: String) {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            assertThat((it.phase as Calling).startingPlayer).isEqualTo(gameGenerator.retrievePlayer(pointer))
        }
    }

    fun raiserIs(pointer: String) {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            assertThat((it.phase as BettingPhase).startingPlayer).isEqualTo(gameGenerator.retrievePlayer(pointer))
        }
    }

    fun currentBetIs(accessor: IntAccessor) {
        val target = accessor()
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            assertThat((it.phase as BettingPhase).currentBet).isEqualTo(target)
        }
    }

    fun deckLost(cardPointer: String) {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            assertThat(it.deck.cards).doesNotContain(
                    RANDOMIZED_CARDS[cardPointerToIndex(cardPointer)]
            )
        }
    }

    fun deckLost(accessor: IntAccessor) {
        val numberOfLostCards = accessor()
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            assertThat(it.deck.size).isEqualTo(gameGenerator.deck().size - numberOfLostCards)
        }
    }

    fun deckLastCardIs(cardPointer: String) {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            assertThat(it.deck.cards.last()).isEqualTo(
                    when (cardPointer) {
                        "targeted card" -> performingContext.targetCard!!
                        else -> throw AssertionError("Card pointer unknown: $cardPointer")
                    }
            )
        }
    }

    fun roundIncreased() {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            assertThat(it.round).isEqualTo(
                    gameGenerator.round() + 1
            )
        }
    }

    fun tableIsSaved() {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            assertThat((gamingTableRepository retrieve it.id).get()).isEqualTo(it)
        }
    }

    fun errorReceived(value: String) {
        assertThat(performingContext.result!!.isLeft).isTrue()
        performingContext.result!!.mapLeft {
            assertThat(it).isInstanceOf(when (value) {
                "not player turn" -> NotPlayerTurn::class.java
                "invalid state" -> InvalidStateForCalledAction::class.java
                "pot building not over" -> PotBuildingNotOver::class.java
                "not enough cards to deal" -> NotEnoughCardsToDeal::class.java
                "not enough credits" -> NotEnoughCredits::class.java
                "card not owned" -> CardNotOwned::class.java
                "card not in interference field" -> CardNotInInterferenceField::class.java
                "bet too low" -> BetTooLow::class.java
                "not enough players" -> NotEnoughPlayers::class.java
                "too many players" -> TooManyPlayers::class.java
                else -> throw AssertionError("Error unknown: $value")
            })
        }
    }

    fun checkPerfomResult() {
        if (performingContext.result == null) {
            throw AssertionError("No action performed")
        }
    }

    fun winner(): PlayerAsserter {
        assertThat(performingContext.result!!.isRight).isTrue()
        return performingContext.result!!.map {
            val winnerId = (performingContext.result!!.get().phase as GameOver).winner
            println("winner id: $winnerId")
            println("it players: ${it.players.content.map { p -> p.id }}")
            PlayerAsserter(
                    this,
                    it.get(winnerId)
            )
        }.get()
    }

    fun loosers(): PlayersAsserter {
        assertThat(performingContext.result!!.isRight).isTrue()
        return performingContext.result!!.map {
            val winnerId = (performingContext.result!!.get().phase as GameOver).winner
            PlayersAsserter(
                    this,
                    it.players.content.filter { it.id != winnerId }
            )
        }.get()
    }

    fun playersInvolved(amount: IntAccessor) {
        assertThat(performingContext.result!!.isRight).isTrue()
        performingContext.result!!.map {
            assertThat((it.phase as SuddenDemisePhase).playersInvolved.size).isEqualTo(amount())
        }
    }
}

class PlayersAsserter(val root: AssertionContext, val players: List<Player>) {
    fun lostCredits(vararg accessors: IntAccessor): PlayersAsserter {
        players.forEach { PlayerAsserter(root, it).lostCredits(*accessors) }
        return this
    }

    fun creditsDidNotChange(): PlayersAsserter {
        players.forEach { assertThat(it.credits).isEqualTo(root.gameGenerator().get(it.id).credits) }
        return this
    }

    fun gainedCard(amount: IntGenerator): PlayersAsserter {
        players.forEach { assertThat(it.handSize).isEqualTo(root.gameGenerator().get(it.id).handSize + amount()) }
        return this
    }

    fun hasHandSize(amount: IntGenerator): PlayersAsserter {
        players.forEach { assertThat(it.handSize).isEqualTo(amount()) }
        return this
    }

    fun handSizeDidNotChange(): PlayersAsserter {
        players.forEach {
            assertThat(it.handSize).isEqualTo(root.gameGenerator().get(it.id).handSize)
        }
        return this
    }

    fun interferenceFieldDidNotChange(): PlayersAsserter {
        players.forEach {
            assertThat(it.interferenceField.cards).containsExactly(*root.gameGenerator().get(it.id).interferenceField.cards.toTypedArray())
        }
        return this
    }

    fun haveDrawnTopOfTheDeck(): PlayersAsserter {
        val playerCards = players.fold(mutableListOf<Card>()) { acc, player ->
            acc.addAll(player.hand.cards)
            acc
        }
        assertThat(playerCards).containsExactlyInAnyOrder(*root.gameGenerator().deck.cards.subList(0, playerCards.size).toTypedArray())
        return this
    }
}

class PlayerAsserter(val root: AssertionContext, val player: Player) {
    fun andGainedCredits(vararg pointers: String): PlayerAsserter {
        var targetValue = root.gameGenerator.retrievePlayerContext(player.id).credits()
        for (pointer in pointers) {
            targetValue += root.gameGenerator.getCredits(pointer)
        }
        assertThat(player.credits).isEqualTo(targetValue)
        return this
    }

    fun andGainedCredits(vararg accessors: IntAccessor): PlayerAsserter {
        var targetValue = root.gameGenerator.retrievePlayerContext(player.id).credits()
        for (accessor in accessors) {
            targetValue += root.accessor()
        }
        assertThat(player.credits).isEqualTo(targetValue)
        return this
    }

    fun won(): PlayerAsserter {
        assertThat((root.performingContext.result!!.get().phase as GameOver).winner)
                .isEqualTo(player.id)
        return this
    }

    fun hasBombedOut(): PlayerAsserter {
        assertThat(player.status).isInstanceOf(BombedOut::class.java)
        return this
    }

    fun isOutOfMoney(): PlayerAsserter {
        assertThat(player.status).isInstanceOf(OutOfMoney::class.java)
        return this
    }

    // TODO: remove this one
    fun lostCredits(vararg pointers: String): PlayerAsserter {
        var targetValue = root.gameGenerator.retrievePlayerContext(player.id).credits()
        for (pointer in pointers) {
            targetValue -= root.gameGenerator.getCredits(pointer)
        }
        assertThat(player.credits).isEqualTo(targetValue)
        return this
    }

    fun lostCredits(vararg accessors: IntAccessor): PlayerAsserter {
        var lostCredits = accessors.fold(0) { accumulator, accessor -> accumulator + root.accessor() }
        var targetValue =
                root.gameGenerator.retrievePlayerContext(player.id).credits() - lostCredits
        assertThat(player.credits).isEqualTo(targetValue)
        return this
    }

    fun interferenceFieldContains(cardPointer: String): PlayerAsserter {
        when (cardPointer) {
            "targeted card" -> assertThat(player.interferenceField.cards).contains(
                    root.performingContext.targetCard!!
            )
            else -> assertThat(player.hand.cards).containsExactly(
                    RANDOMIZED_CARDS[cardPointerToIndex(cardPointer)]
            )
        }
        return this
    }

    fun interferenceFieldDoesNotContain(cardPointer: String): PlayerAsserter {
        when (cardPointer) {
            "targeted card" -> assertThat(player.interferenceField.cards).doesNotContain(
                    root.performingContext.targetCard!!
            )
            else -> assertThat(player.hand.cards).doesNotContain(
                    RANDOMIZED_CARDS[cardPointerToIndex(cardPointer)]
            )
        }
        return this
    }

    fun handContains(cardPointer: String): PlayerAsserter {
        when (cardPointer) {
            "targeted card" -> assertThat(player.hand.cards).contains(
                    root.performingContext.targetCard!!
            )
            else -> assertThat(player.hand.cards).contains(
                    RANDOMIZED_CARDS[cardPointerToIndex(cardPointer)]
            )
        }
        return this
    }

    fun handDoesNotContains(cardPointer: String): PlayerAsserter {
        when (cardPointer) {
            "targeted card" -> assertThat(player.hand.cards).doesNotContain(
                    root.performingContext.targetCard!!
            )
            else -> assertThat(player.hand.cards).doesNotContain(
                    RANDOMIZED_CARDS[cardPointerToIndex(cardPointer)]
            )
        }
        return this
    }

    fun handDidNotChange(): PlayerAsserter {
        assertThat(player.hand.cards).containsExactly(
                *root.gameGenerator.retrievePlayerContext(player.id).hand.cards.toTypedArray()
        )
        return this
    }

    fun hasTheBestScore(): PlayerAsserter {
        root.performingContext.result!!.get().players.content.forEach {
            if (it.id != player.id) {
                player.getScore() > it.getScore()
            }
        }
        return this
    }

}

// TODO: update deck : should not be the randomized one, but a real deck
private fun cardPointerToIndex(cardPointer: String) = when (cardPointer) {
    "first deck card" -> 0
    "last deck card" -> RANDOMIZED_CARDS.size - 1
    else -> cardPointer.toInt()
}
