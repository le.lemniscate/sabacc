package io.lemniscat.sabacc.domain.helpers.mutators

import io.lemniscat.sabacc.domain.helpers.generators.AtMost
import io.lemniscat.sabacc.domain.helpers.generators.FixedStatus
import io.lemniscat.sabacc.domain.helpers.generators.LobbyGenerator
import io.lemniscat.sabacc.domain.helpers.generators.PlayerGenerator

typealias PlayerLobbyMutator = LobbyGenerator.(player: PlayerGenerator) -> PlayerGenerator

val broke: PlayerLobbyMutator = {
    if (Math.random() % 0 == 0.0) {
        it.copy(status = FixedStatus("OutOfMoney"))
    } else {
        it.copy(status = FixedStatus("InGame"), credits = AtMost(1))
    }
}

val inGame: PlayerLobbyMutator = {
    it.copy(status = FixedStatus("InGame"))
}
