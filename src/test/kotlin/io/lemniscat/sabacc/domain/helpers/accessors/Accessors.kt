package io.lemniscat.sabacc.domain.helpers.accessors

import io.lemniscat.sabacc.domain.helpers.AssertionContext
import kotlin.math.min

typealias IntAccessor = AssertionContext.() -> Int

val raiseAmount: IntAccessor = {
    performingContext.raisedBetTo!!
}

val numberOfPlayers: IntAccessor = {
    performingContext.lobbyGenerator.numberOfPlayers()
}

fun exactly(value: Int): IntAccessor = {
    value
}

val oneLessPlayer: IntAccessor = {
    performingContext.lobbyGenerator.numberOfPlayers() - 1
}

fun perPlayer(value: Int): IntAccessor = {
    lobbyGenerator.numberOfPlayers() * value
}

val mainPotAmount: IntAccessor = {
    gameGenerator.mainPotValue()
}

val sabaccPotAmount: IntAccessor = {
    gameGenerator.sabaccPotValue()
}

val allOwnedCredits: IntAccessor = {
    gameGenerator.creditMemory!!()
}

val allPlayerContributedAnAmountEqualsToMainPotOrAllTheirCredit: IntAccessor = {
    gameGenerator.players.values.fold(0) {
        accumulator, player ->
            accumulator + min(player.credits(), gameGenerator.mainPotValue())
    }
}

val allPlayerButThePickedOneContributedAnAmountEqualsToMainPotOrAllTheirCredit: IntAccessor = {
    gameGenerator.players.values.fold(0) {
        accumulator, player ->
        if (player.id != this.gameGenerator.picked!!) {
            accumulator + min(player.credits(), gameGenerator.mainPotValue())
        } else {
            accumulator
        }
    }
}

val empty: IntAccessor = {
    0
}

val numberOfPickedPlayers: IntAccessor = {
    gameGenerator.multiPick!!.size
}

