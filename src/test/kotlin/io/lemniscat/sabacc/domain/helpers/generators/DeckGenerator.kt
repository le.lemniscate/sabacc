package io.lemniscat.sabacc.domain.helpers.generators

import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.card.Deck
import kotlin.random.Random

abstract class DeckGenerator {
    abstract var cards: List<Card>

    abstract operator fun invoke(): Deck

    fun shuffle() {
        cards = cards.shuffled()
    }
}

class RandomDeck: DeckGenerator() {
    override var cards: List<Card> = Deck.shuffle(Random(13)).cards

    override fun invoke(): Deck =
        Deck.of(cards)
}

class EmptyDeck: DeckGenerator() {
    override var cards = listOf<Card>()

    override fun invoke(): Deck =
        Deck.of(cards)
}
