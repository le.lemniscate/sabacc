package io.lemniscat.sabacc.domain.helpers.generators

import io.lemniscat.sabacc.domain.helpers.mutators.PlayerLobbyMutator
import io.lemniscat.sabacc.domain.model.Lobby
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.player.Players

class LobbyGenerator {
    var numberOfPlayers: IntGenerator = Fixed(0)
    var players = mutableMapOf<Id, PlayerGenerator>()

    operator fun invoke(): Lobby {
        return Lobby.empty().copy(
                users = listOf()
        )
    }

    fun joinedBy(amount: IntGenerator) {
        players = mutableMapOf()
        numberOfPlayers = amount
        for (i in 0 until numberOfPlayers()) {
            val player = PlayerGenerator()
            players.put(player.id, player)
        }
    }

    fun all(vararg mutators: PlayerLobbyMutator) {
        mutators.forEach { mutator ->
            players.keys.forEach { applyMutator(it, mutator) }
        }
    }

    fun one(vararg mutators: PlayerLobbyMutator) {
        val target = players.keys.shuffled().first()
        mutators.forEach { mutator ->
            applyMutator(target, mutator)
        }
    }

    private fun applyMutator(id: Id, playerMutator: PlayerLobbyMutator) {
        if (players[id] != null) {
            players[id] = this.playerMutator(players[id]!!)
        } else {
            throw AssertionError("Trying to edit inexisting $id")
        }
    }

}