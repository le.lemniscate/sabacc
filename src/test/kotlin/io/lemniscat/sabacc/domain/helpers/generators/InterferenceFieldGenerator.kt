package io.lemniscat.sabacc.domain.helpers.generators

import io.lemniscat.sabacc.domain.helpers.factories.any
import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.player.InterferenceField

abstract class InterferenceFieldGenerator {
    abstract val cards: List<Card>

    abstract operator fun invoke(): InterferenceField
    abstract fun getRandomCard(): Card
}

class EmptyInterferenceField: InterferenceFieldGenerator() {
    override val cards = listOf<Card>()

    override fun invoke(): InterferenceField = InterferenceField.empty()

    override fun getRandomCard(): Card {
        throw AssertionError("Cannot get a random card on an empty interference field")
    }
}

class InterferenceFieldOfSize: InterferenceFieldGenerator {
    override val cards: MutableList<Card> = mutableListOf()

    constructor(value: IntGenerator): super() {
        for (i in 0 until value()) {
            cards.add(Card.any())
        }
    }

    override fun getRandomCard(): Card =
            cards.shuffled().first()

    override fun invoke(): InterferenceField = InterferenceField.of(cards)
}
