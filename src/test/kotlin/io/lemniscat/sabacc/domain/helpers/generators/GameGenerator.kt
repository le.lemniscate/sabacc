package io.lemniscat.sabacc.domain.helpers.generators

import io.lemniscat.sabacc.domain.helpers.mutators.PlayerMutator
import io.lemniscat.sabacc.domain.helpers.mutators.insertCardOfValue
import io.lemniscat.sabacc.domain.helpers.mutators.outOfGame
import io.lemniscat.sabacc.domain.model.GamingTable
import io.lemniscat.sabacc.domain.model.Pot
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.player.Players
import kotlin.AssertionError
import kotlin.random.Random

class GameGenerator {
    var currentPlayerId: Id
    var picked: Id? = null
    var multiPick: List<Id>? = null
    val players: MutableMap<Id, PlayerGenerator> = mutableMapOf()
    var betLevel: IntGenerator = Fixed(0)
    var creditMemory: IntGenerator? = null
    var scoreMemory: IntGenerator? = null
    var drawValue: IntGenerator? = null
    var phase: PhaseGenerator = FixedPhase("Betting")
    var deck: DeckGenerator = RandomDeck()
    var round: IntGenerator = AtLeast(0)
    var mainPotValue: IntGenerator = AtLeast(0)
    var sabaccPotValue: IntGenerator = AtLeast(0)

    constructor() {
        val numberOfPlayers = correctAmountOfPlayers()
        for (i in 0 until numberOfPlayers) {
            val player = PlayerGenerator()
            players[player.id] = player
        }
        currentPlayerId = players.keys.shuffled().first()
    }

    operator fun invoke(): GamingTable {
        val players = Players.of(players.values.toList().map { it() })
        return GamingTable.empty().copy(
                players = players,
                round = round(),
                phase = phase(players, currentPlayerId),
                mainPot = Pot.empty() + mainPotValue(),
                sabaccPot = Pot.empty() + sabaccPotValue(),
                deck = deck()
        )
    }

    fun numberOfPlayers(intGenerator: IntGenerator) {
        val numberOfPlayers = intGenerator()
        for (i in 0 until numberOfPlayers) {
            val player = PlayerGenerator()
            players[player.id] = player
        }
        currentPlayerId = players.keys.shuffled().first()
    }

    fun stillInGame(vararg pointers: Any) {
        applyToAll(outOfGame)
        for (pointer in pointers) {
            when (pointer) {
                is String -> applyMutator(retrievePlayer(pointer), io.lemniscat.sabacc.domain.helpers.mutators.stillInGame)
                is Id -> applyMutator(pointer, io.lemniscat.sabacc.domain.helpers.mutators.stillInGame)
                else -> throw AssertionError("Invalid type of pointer received: $pointer")
            }
        }
    }

    fun pick(pointer: String): Id {
        picked = retrievePlayer(pointer)
        return picked!!
    }

    fun pick(amount: GameContextBoundIntGenerator) {
        multiPick = players.keys.shuffled().subList(0, amount()())
    }

    fun startSuddenDemiseWith(pointer: String) {
        when (pointer) {
            "picked" -> {
                phase("SuddenDemise")
                phase.addMutator(playerInvolved(multiPick!!))
            }
            "all" -> {
                phase("SuddenDemise")
                phase.addMutator(playerInvolved(players.keys.toList()))
            }
            "all but at least two" -> {
                phase("SuddenDemise")
                val shuffledPlayersId = players.keys.toList().shuffled()
                if (shuffledPlayersId.size == 2) {
                    shuffledPlayersId
                } else {
                    phase.addMutator(playerInvolved(shuffledPlayersId.subList(2, shuffledPlayersId.size)))
                }
            }
            else -> throw AssertionError("Invalid type of pointer received: $pointer")
        }
    }

    fun retrievePlayerContext(id: Id): PlayerGenerator {
        return players[id]!!
    }

    fun retrievePlayer(pointer: String): Id {
        return when (pointer) {
            "any player" -> players.keys.shuffled().first()
            "current player" -> currentPlayerId
            "any but current player" -> players.keys.filter { it != currentPlayerId }.shuffled().first()
            "next player" -> {
                val allPlayers = players.keys.toList()
                var nextPlayerIndex = allPlayers.indexOf(currentPlayerId) + 1
                nextPlayerIndex = if (nextPlayerIndex >= allPlayers.size) {
                    0
                } else {
                    nextPlayerIndex
                }
                return allPlayers[nextPlayerIndex]
            }
            "picked" -> if (picked != null) {
                picked!!
            } else {
                throw AssertionError("No player was picked")
            }
            "any but picked" -> if (picked != null) {
                players.keys.filter { it != picked }.shuffled().first()
            } else {
                throw AssertionError("No player was picked")
            }
            else -> throw AssertionError("Invalid player pointer: $pointer")
        }
    }

    fun getCredits(pointer: String): Int {
        return when (pointer) {
            "main pot" -> mainPotValue()
            "sabacc pot" -> sabaccPotValue()
            "betting value" -> betLevel()
            else -> throw AssertionError("Invalid pot pointer: $pointer")
        }
    }

    fun phase(generator: PhaseGenerator) {
        phase = generator
    }

    fun phase(target: String) {
        phase = FixedPhase(target)
    }

    fun deck(target: String) {
        when (target) {
            "is empty" -> deck = EmptyDeck()
            else -> deck = RandomDeck()
        }
    }

    fun round(generator: IntGenerator) {
        round = generator
    }

    fun startedBy(pointer: String) {
        phase.addMutator(phaseStartedBy(retrievePlayer(pointer)))
    }

    fun raisedBet(id: Id) {
        phase.addMutator(betRaisedBy(id))
    }

    fun raisedBetTo(value: IntGenerator) {
        betLevel = value
        phase.addMutator(betRaisedTo(betLevel()))
    }

    fun currentPlayer(vararg playerMutator: PlayerMutator) {
        for (mutator in playerMutator) {
            applyMutator(currentPlayerId, mutator)
        }
    }

    fun pickedPlayer(vararg playerMutator: PlayerMutator) {
        for (mutator in playerMutator) {
            applyMutator(picked!!, mutator)
        }
    }

    fun allPlayersBut(pointer: String) = { mutator: PlayerMutator ->
        val target = retrievePlayer(pointer)
        players.keys.forEach {
            if (it != target) {
                applyMutator(it, mutator)
            }
        }
    }

    fun whenDrawing(callback: GameGenerator.() -> Unit) {
        deck = EmptyDeck()
        callback()
        deck.shuffle()
    }

    // TODO: following should be available only in the whenDrawing
    fun allPlayerWillGet(target: GameContextBoundIntGenerator) {
        for (i in 0 until players.size) {
            val mutator = insertCardOfValue(target()())
            deck.mutator()
        }
    }

    fun onePlayerWillGet(target: GameContextBoundIntGenerator) {
        val mutator = insertCardOfValue(target()())
        deck.mutator()
    }

    fun playersWillBeTied(amount: String) {
        multiPick = when (amount) {
            "some" -> players.keys.shuffled().subList(0,
                if (players.size > 2) {
                    Random.nextInt(2, players.size)
                } else {
                    2
                }
            )
            "leave at least one" -> players.keys.shuffled().subList(0,
                    if (players.size > 2) {
                        Random.nextInt(2, players.size - 1)
                    } else {
                        1
                    }
            )
            else -> throw AssertionError("Unknown amount $amount")
        }
        val insertedCardValue = Between(1, 23 - scoreMemory!!())
        val mutator = insertCardOfValue(insertedCardValue())
        drawValue = insertedCardValue
        for (i in 0 until multiPick!!.size) {
            deck.mutator()
        }
    }

    fun whileOtherWillGet(target: GameContextBoundIntGenerator) {
        var numberOfPlayerNotInvolved = 1
        if (multiPick != null) {
            numberOfPlayerNotInvolved = multiPick!!.size
        }
        for (i in 0 until players.size - numberOfPlayerNotInvolved) {
            val mutator = insertCardOfValue(target()())
            deck.mutator()
        }
    }

    fun allPlayers(vararg playerMutators: PlayerMutator) {
        players.keys.forEach {
            playerMutators.forEach { mutator ->
                applyMutator(it, mutator)
            }
        }
    }

    fun anyButCurrentPlayer(vararg playerMutator: PlayerMutator) {
        val target = retrievePlayer("any but current player")
        for (mutator in playerMutator) {
            applyMutator(target, mutator)
        }
    }

    fun anyButPickedPlayer(vararg playerMutator: PlayerMutator) {
        val target = retrievePlayer("any but picked")
        for (mutator in playerMutator) {
            applyMutator(target, mutator)
        }
    }

    fun nextPlayer(vararg playerMutator: PlayerMutator) {
        for (mutator in playerMutator) {
            applyMutator(retrievePlayer("next player"), mutator)
        }
    }

    private fun applyMutator(id: Id, playerMutator: PlayerMutator) {
        if (players[id] != null) {
            players[id] = this.playerMutator(players[id]!!)
        } else {
            throw AssertionError("Trying to edit inexisting $id")
        }
    }

    private fun applyToAll(playerMutator: PlayerMutator) {
        for (id in players.keys) {
            players[id] = this.playerMutator(players[id]!!)
        }
    }
}
