package io.lemniscat.sabacc.domain.helpers.generators

import io.lemniscat.sabacc.domain.helpers.factories.*
import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.player.Hand

abstract class HandGenerator {
    abstract val cards: List<Card>

    abstract operator fun invoke(): Hand
    abstract fun getRandomCard(): Card
}

class EmptyHand: HandGenerator() {
    override val cards = listOf<Card>()

    override fun invoke(): Hand = Hand.empty()

    override fun getRandomCard(): Card {
        throw AssertionError("Cannot get a random card on an empty hand")
    }
}

class HandOfSize: HandGenerator {
    override val cards: MutableList<Card> = mutableListOf()

    constructor(value: IntGenerator): super() {
        for (i in 0 until value()) {
            cards.add(Card.any())
        }
    }

    override fun getRandomCard(): Card =
            cards.shuffled().first()

    override fun invoke(): Hand = Hand.of(cards)
}

class MoreOrLessThan23: HandGenerator {
    override val cards: MutableList<Card> = mutableListOf()

    constructor(): super() {
        var value = 0
        while (value < 23 && value > -23) {
            val card = Card.any()
            value += card.value.points
            cards.add(card)
        }
    }

    override fun getRandomCard(): Card = cards.shuffled().first()

    override fun invoke(): Hand = Hand.of(cards)
}

class ScoringZero: HandGenerator {
    override val cards: MutableList<Card> = mutableListOf()

    constructor(): super() {
        val hand = Hand.zeroBombOut()
        cards.addAll(hand.cards)
    }

    override fun getRandomCard(): Card = cards.shuffled().first()

    override fun invoke(): Hand = Hand.of(cards)
}

class IdiotArrayHand: HandGenerator {
    override val cards: MutableList<Card> = mutableListOf()

    constructor(): super() {
        val hand = Hand.idiotsArray()
        cards.addAll(hand.cards)
    }

    override fun getRandomCard(): Card = cards.shuffled().first()

    override fun invoke(): Hand = Hand.of(cards)
}

class PureSabaccHand: HandGenerator {
    override val cards: MutableList<Card> = mutableListOf()

    constructor(): super() {
        val hand = Hand.pureSabacc()
        cards.addAll(hand.cards)
    }

    override fun getRandomCard(): Card = cards.shuffled().first()

    override fun invoke(): Hand = Hand.of(cards)
}

class NegativePureSabaccHand: HandGenerator {
    override val cards: MutableList<Card> = mutableListOf()

    constructor(): super() {
        val hand = Hand.negativePureSabacc()
        cards.addAll(hand.cards)
    }

    override fun getRandomCard(): Card = cards.shuffled().first()

    override fun invoke(): Hand = Hand.of(cards)
}

class NormalHand: HandGenerator {
    override val cards: MutableList<Card> = mutableListOf()

    constructor(value: Int = Between(-22, 22)()): super() {
        val hand = if (value < 0) {
            Hand.negative(value)
        } else {
            Hand.positive(value) // To avoid 0 value and allow 22 value
        }
        cards.addAll(hand.cards)
    }

    override fun getRandomCard(): Card = cards.shuffled().first()

    override fun invoke(): Hand = Hand.of(cards)
}
