package io.lemniscat.sabacc.domain.helpers.generators

import io.lemniscat.sabacc.domain.helpers.factories.random
import io.lemniscat.sabacc.domain.helpers.factories.randomString
import io.lemniscat.sabacc.domain.model.ORDERED_CARDS
import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.player.Player
import io.lemniscat.sabacc.domain.model.player.PlayerName

data class PlayerGenerator(
        var id: Id = Id.of(randomString(42)),
        var status: PlayerStatusGenerator = FixedStatus("InGame"),
        var credits: IntGenerator = AtLeast(0),
        var hand: HandGenerator = EmptyHand(),
        var interferenceField: InterferenceFieldGenerator = EmptyInterferenceField()
) {
    operator fun invoke(): Player = Player.empty().copy(
            id = id,
            name = PlayerName.random(),
            credits = credits(),
            status = status(),
            hand = hand(),
            interferenceField = interferenceField()
    )

    fun retrieveCard(pointer: String): Card {
        return when (pointer) {
            "any card" -> hand.getRandomCard()
            "any not owned card" -> ORDERED_CARDS.filter { !(it in hand.cards) }.shuffled().first()
            else -> throw AssertionError("Cannot retrieve card $pointer in hand")
        }
    }

    fun retrieveCardInInterferenceField(pointer: String): Card {
        return when (pointer) {
            "any card" -> interferenceField.getRandomCard()
            "any not interfered card" -> ORDERED_CARDS.filter { !(it in interferenceField.cards) }.shuffled().first()
            else -> throw AssertionError("Cannot retrieve card $pointer in interference field")
        }
    }
}