package io.lemniscat.sabacc.domain.helpers.factories

import io.lemniscat.sabacc.domain.model.player.PlayerName

fun PlayerName.Factories.random() = of(randomString())
