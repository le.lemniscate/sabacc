package io.lemniscat.sabacc.domain.helpers

import io.lemniscat.sabacc.domain.helpers.generators.GameGenerator
import io.lemniscat.sabacc.domain.helpers.generators.UUIDGenerator
import io.lemniscat.sabacc.domain.helpers.generators.LobbyGenerator
import io.lemniscat.sabacc.infra.repositories.GamingTableMemoryRepository

class GivenWhenThen {
    val gamingTableRepository = GamingTableMemoryRepository(UUIDGenerator())

    val gameContext = GameGenerator()
    val lobbyContext = LobbyGenerator()
    val performingContext = PerformingContext(gamingTableRepository, gameContext, lobbyContext)
    val assertionContext = AssertionContext(gamingTableRepository, gameContext, lobbyContext, performingContext)
}

fun given(initializer: GameGenerator.() -> Unit): GivenWhenThen {
    val result = GivenWhenThen()
    result.gameContext.initializer()
    return result
}

fun givenLobby(initializer: LobbyGenerator.() -> Unit): GivenWhenThen {
    val result = GivenWhenThen()
    result.lobbyContext.initializer()
    return result
}

fun GivenWhenThen.on(perform: PerformingContext.() -> Unit): GivenWhenThen {
    performingContext.perform()
    return this
}

fun GivenWhenThen.then(assert: AssertionContext.() -> Unit): GivenWhenThen {
    assertionContext.checkPerfomResult()
    assertionContext.assert()
    return this
}
