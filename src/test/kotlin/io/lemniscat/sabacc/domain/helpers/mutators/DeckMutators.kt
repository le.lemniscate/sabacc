package io.lemniscat.sabacc.domain.helpers.mutators

import io.lemniscat.sabacc.domain.helpers.factories.ofValue
import io.lemniscat.sabacc.domain.helpers.generators.DeckGenerator
import io.lemniscat.sabacc.domain.model.card.Card

typealias DeckMutator = DeckGenerator.() -> DeckGenerator

fun insertCardOfValue(target: Int): DeckMutator = {
    cards = cards + Card.ofValue(target)
    this
}