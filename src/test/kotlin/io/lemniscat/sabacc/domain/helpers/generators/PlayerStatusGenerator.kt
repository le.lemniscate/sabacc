package io.lemniscat.sabacc.domain.helpers.generators

import io.lemniscat.sabacc.domain.model.player.PlayerStatus

interface PlayerStatusGenerator {
    operator fun invoke(): PlayerStatus

    companion object {
        fun from(value: String): PlayerStatus {
            return when (value) {
                "InGame" -> PlayerStatus.inGame()
                "Fold" -> PlayerStatus.fold()
                "OutOfMoney" -> PlayerStatus.outOfMoney()
                "BombedOut" -> PlayerStatus.bombedOut()
                "Won" -> PlayerStatus.won()
                else -> throw AssertionError("Illegal Player Status asked")
            }
        }
    }
}

data class FixedStatus(val status: String): PlayerStatusGenerator {
    override operator fun invoke(): PlayerStatus {
        return PlayerStatusGenerator.from(status)
    }
}

class AnyStatusBetween(vararg statuses: String): PlayerStatusGenerator {
    private val possibleStatuses = statuses.asList()

    override operator fun invoke(): PlayerStatus {
        return PlayerStatusGenerator.from(possibleStatuses.shuffled().first())
    }
}
