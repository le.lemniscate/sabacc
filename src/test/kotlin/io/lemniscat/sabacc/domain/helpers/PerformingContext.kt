package io.lemniscat.sabacc.domain.helpers

import io.lemniscat.sabacc.domain.helpers.generators.*
import io.lemniscat.sabacc.domain.model.card.Card
import io.lemniscat.sabacc.domain.model.misc.LobbyResult
import io.lemniscat.sabacc.domain.model.misc.UseCaseResult
import io.lemniscat.sabacc.domain.repositories.GamingTableRepository
import io.lemniscat.sabacc.domain.usecases.game.*
import kotlin.random.Random

class PerformingContext(
        val gamingTableRepository: GamingTableRepository,
        val gameGenerator: GameGenerator,
        val lobbyGenerator: LobbyGenerator
) {
    var result: UseCaseResult? = null
    var lobbyResult: LobbyResult? = null
    var targetCard: Card? = null
    var raisedBetTo: Int? = null

    fun startGame() {
        result = StartGame(gamingTableRepository, Random(13))(
                lobbyGenerator()
        )
    }

    fun fold(target: String) {
        result = Fold(gamingTableRepository)(
                gameGenerator(),
                gameGenerator.retrievePlayer(target)
        )
    }

    fun call(target: String) {
        result = Call(gamingTableRepository)(
                gameGenerator(),
                gameGenerator.retrievePlayer(target)
        )
    }

    fun draw(target: String) {
        result = Draw(gamingTableRepository)(
                gameGenerator(),
                gameGenerator.retrievePlayer(target)
        )
    }

    fun matchBet(target: String) {
        result = MatchBet(gamingTableRepository)(
                gameGenerator(),
                gameGenerator.retrievePlayer(target)
        )
    }

    fun raiseBet(target: String, amount: GameContextBoundIntGenerator) =
            raiseBet(target, gameGenerator.amount())

    fun raiseBet(target: String, amount: IntGenerator) {
        raisedBetTo = amount()
        result = RaiseBet(gamingTableRepository)(
                gameGenerator(),
                gameGenerator.retrievePlayer((target)),
                amount()
        )
    }

    fun putInInterferenceField(target: String, cardPointer: String) {
        val player = gameGenerator.retrievePlayer(target)
        targetCard = gameGenerator.retrievePlayerContext(player).retrieveCard(cardPointer)
        result = PutInInterferenceField(gamingTableRepository)(
                gameGenerator(),
                player,
                targetCard!!
        )
    }

    fun removeFromInterferenceField(target: String, cardPointer: String) {
        val player = gameGenerator.retrievePlayer(target)
        targetCard = gameGenerator.retrievePlayerContext(player).retrieveCardInInterferenceField(cardPointer)
        result = RemoveFromInterferenceField(gamingTableRepository)(
                gameGenerator(),
                player,
                targetCard!!
        )
    }

    fun stand(target: String) {
        result = Stand(gamingTableRepository)(
                gameGenerator(),
                gameGenerator.retrievePlayer(target)
        )
    }

    fun trade(target: String, cardPointer: String) {
        val player = gameGenerator.retrievePlayer(target)
        targetCard = gameGenerator.retrievePlayerContext(player).retrieveCard(cardPointer)
        result = Trade(gamingTableRepository)(
                gameGenerator(),
                player,
                targetCard!!
        )
    }

    fun revealCards() {
        result = RevealCards(gamingTableRepository)(
                gameGenerator()
        )
    }

    fun sabaccShift() {
        result = SabaccShift(gamingTableRepository)(
                gameGenerator()
        )
    }

    fun suddenDemise() {
        result = SuddenDemise(gamingTableRepository)(
                gameGenerator()
        )
    }
}
