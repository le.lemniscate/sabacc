package io.lemniscat.sabacc.domain.helpers.factories

import io.lemniscat.sabacc.domain.model.card.*
import io.lemniscat.sabacc.domain.model.player.Hand
import kotlin.math.min
import kotlin.random.Random

operator fun Int.times(card: Card): List<Card> {
    val result = mutableListOf<Card>()
    for (i in 0 until this) {
        result += card
    }
    return result
}

fun Hand.Factories.pureSabacc(): Hand =
        of(
                SuitCard(StandardSuit.Sabers, SuitValue.TEN),
                SuitCard(StandardSuit.Flasks, SuitValue.TEN),
                SuitCard(StandardSuit.Sabers, SuitValue.THREE)
        )

fun Hand.Factories.negativePureSabacc(): Hand =
        of(
                FaceCard(FaceSuit.Black, FaceValue.MODERATION),
                FaceCard(FaceSuit.Black, FaceValue.BALANCE),
                SuitCard(StandardSuit.Sabers, SuitValue.TWO)
        )

fun Hand.Factories.idiotsArray(): Hand =
        of(
                FaceCard(FaceSuit.Black, FaceValue.IDIOT),
                SuitCard(StandardSuit.Flasks, SuitValue.TWO),
                SuitCard(StandardSuit.Flasks, SuitValue.THREE)
        )

fun Hand.Factories.zeroBombOut(): Hand =
        of()

fun Hand.Factories.negative(target: Int): Hand {
    var cards = listOf<Card>()
    var currentValue = 0
    val faceValuesList = FaceValue.values().toList()
    while (currentValue > target) {
        val generatedCard = FaceCard(anyFaceSuit(), faceValuesList.shuffled().first())
        cards = cards + generatedCard
        currentValue += generatedCard.value.points
    }
    while (currentValue != target) {
        val generatedValue = Random.nextInt(min(target - currentValue, MAX_SUIT_VALUE)) + 1
        cards = cards + Card.ofValue(generatedValue)
        currentValue += generatedValue
    }
    return of(cards)
}

fun Hand.Factories.positive(target: Int): Hand {
    var cards = listOf<Card>()
    var currentValue = 0
    while (currentValue != target) {
        val generatedValue = Random.nextInt(min(target - currentValue, MAX_SUIT_VALUE)) + 1
        cards = cards + Card.ofValue(generatedValue)
        currentValue += generatedValue
    }
    return of(cards)
}
