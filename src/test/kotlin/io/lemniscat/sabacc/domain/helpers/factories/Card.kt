package io.lemniscat.sabacc.domain.helpers.factories

import io.lemniscat.sabacc.domain.model.ORDERED_CARDS
import io.lemniscat.sabacc.domain.model.card.*
import kotlin.random.Random

fun Card.Factories.any(): Card =
        ORDERED_CARDS.shuffled().first()

fun Card.Factories.ofValue(target: Int): Card =
    if (target > 0) {
        SuitCard(anySuit(), suitValueOf(target))
    } else {
        FaceCard(anyFaceSuit(), faceValueOf(target))
    }

fun suitValueOf(intValue: Int) = SuitValue.values().first { it.points == intValue }
fun faceValueOf(intValue: Int) = FaceValue.values().first { it.points == intValue }

fun anyFaceSuit() =
        when (Random.nextInt(2)) {
            0 -> FaceSuit.Black
            else -> FaceSuit.White
        }

fun anySuit() =
        when (Random.nextInt(4)) {
            0 -> StandardSuit.Coins
            1 -> StandardSuit.Flasks
            2 -> StandardSuit.Sabers
            else -> StandardSuit.Staves
        }
