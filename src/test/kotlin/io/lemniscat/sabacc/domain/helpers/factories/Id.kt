package io.lemniscat.sabacc.domain.helpers.factories

import io.lemniscat.sabacc.domain.model.misc.Id

fun Id.Factories.random() = of(randomString())
