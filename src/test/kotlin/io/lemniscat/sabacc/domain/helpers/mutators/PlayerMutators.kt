package io.lemniscat.sabacc.domain.helpers.mutators

import io.lemniscat.sabacc.domain.helpers.generators.*

typealias PlayerMutator = GameGenerator.(player: PlayerGenerator) -> PlayerGenerator

val outOfGame: PlayerMutator = {
    it.copy(status = AnyStatusBetween("Fold", "OutOfMoney", "BombedOut"))
}

val fold: PlayerMutator = {
    it.copy(status = FixedStatus("Fold"))
}

val stillInGame: PlayerMutator = {
    it.copy(status = FixedStatus("InGame"))
}

val raisedBet: PlayerMutator = {
    this.raisedBet(it.id)
    it
}

val cannotMatchBet: PlayerMutator = {
    withCredits(AtMost(this.betLevel()))(it)
}

val canMatchBet: PlayerMutator = {
    withCredits(AtLeast(this.betLevel()))(it)
}

val canOvercomeBet: PlayerMutator = {
    withCredits(AtLeast(this.betLevel() + 1))(it)
}

fun withHandSize(size: IntGenerator): PlayerMutator = {
    it.copy(hand = HandOfSize(size))
}

fun withInterferenceFieldSize(size: IntGenerator): PlayerMutator = {
    it.copy(interferenceField = InterferenceFieldOfSize(size))
}

val canMatchMainPot: PlayerMutator = {
    withCredits(this.mainPotValue)(it)
}

val cannotMatchMainPot: PlayerMutator = {
    withCredits(AtMost(mainPotValue()))(it)
}

fun withCredits(amount: IntGenerator): PlayerMutator = {
    this.creditMemory = amount
    it.copy(credits = amount)
}

fun withScore(value: IntGenerator): PlayerMutator = {
    scoreMemory = value
    it.copy(hand = NormalHand(value()))
}

fun withScoreLessThat(value: GameContextBoundIntGenerator): PlayerMutator = {
    val targetScore = Math.abs(value()())
    it.copy(hand = NormalHand(PositiveOrNegativeBetween(1, targetScore)()))
}

val opositeThanPickedScore: PlayerMutator = {
    it.copy(hand = NormalHand(-scoreMemory!!()))
}

val equalToPickedScore: PlayerMutator = {
    it.copy(hand = NormalHand(scoreMemory!!()))
}

fun withHand(hand: HandGenerator): PlayerMutator = {
    it.copy(hand = hand)
}
