package io.lemniscat.sabacc.domain.helpers.generators

import io.lemniscat.sabacc.domain.model.misc.Id
import io.lemniscat.sabacc.domain.model.phases.*
import io.lemniscat.sabacc.domain.model.player.Players

typealias PhaseMutator = GamePhase.() -> GamePhase

fun betRaisedBy(id: Id): PhaseMutator = {
    when (this) {
        is Betting -> this.copy(startingPlayer = id)
        is Calling -> this.copy(startingPlayer = id)
        else -> throw AssertionError("Phase $this cannot be raised")
    }
}

fun betRaisedTo(value: Int): PhaseMutator = {
        when (this) {
            is Betting -> this.copy(currentBet = value)
            is Calling -> this.copy(currentBet = value)
            else -> throw java.lang.AssertionError("Phase $this is not a betting phase")
        }
}

fun phaseStartedBy(id: Id): PhaseMutator = {
    when (this) {
        is Dealing -> this.copy(startingPlayer = id)
        is Betting -> this.copy(startingPlayer = id)
        is Calling -> this.copy(startingPlayer = id)
        else -> throw AssertionError("Phase $this is not a turn-based phase")
    }
}

fun playerInvolved(ids: List<Id>): PhaseMutator = {
    when (this) {
        is SuddenDemisePhase -> this.copy(playersInvolved = ids)
        else -> throw AssertionError("Phase $this is not Sudden Demise")
    }
}

abstract class PhaseGenerator {
    val mutators: MutableList<PhaseMutator> = mutableListOf()

    fun addMutator(phaseMutator: PhaseMutator) {
        mutators.add(phaseMutator)
    }

    fun applyMutators(phase: GamePhase): GamePhase {
        var result = phase
        for (mutator in mutators) {
            result = result.mutator()
        }
        return result
    }

    abstract operator fun invoke(players: Players, currentPlayer: Id): GamePhase

    val allPhases = listOf("Betting", "Calling", "Dealing", "GameOver", "GameStarted", "Revealing", "SuddenDemise")

    fun from(value: String, players: Players, currentPlayer: Id): GamePhase {
        return when (value) {
            "Betting" -> Betting.start(players, currentPlayer)
            "Calling" -> Calling.start(players, currentPlayer)
            "Dealing" -> Dealing.start(players, currentPlayer)
            "GameOver" -> GameOver.start(currentPlayer)
            "GameStarted" -> GameStarted.start()
            "Revealing" -> Revealing.start()
            "SuddenDemise" -> SuddenDemisePhase.start(players.content.map { it.id })
            else -> throw AssertionError("Asked for invalid phase: $value")
        }
    }
}

class FixedPhase(val phase: String): PhaseGenerator() {
    override operator fun invoke(players: Players, currentPlayer: Id): GamePhase {
        return applyMutators(from(phase, players, currentPlayer))
    }
}

class AnyBut(vararg phases: String): PhaseGenerator() {
    val phases = phases.toList()

    override operator fun invoke(players: Players, currentPlayer: Id): GamePhase {
        return applyMutators(from(allPhases.filter { !(it in phases) }.shuffled().first(), players, currentPlayer))
    }
}

class AnyOf(vararg phases: String): PhaseGenerator() {
    val phases = phases.toList()

    override fun invoke(players: Players, currentPlayer: Id): GamePhase {
        return applyMutators(from(phases.shuffled().first(), players, currentPlayer))
    }
}
