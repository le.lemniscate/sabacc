package io.lemniscat.sabacc.domain.helpers.generators

import kotlin.random.Random

typealias GameContextBoundIntGenerator = GameGenerator.() -> IntGenerator

val moreThanOwnedCredits: GameContextBoundIntGenerator = {
    AtLeast(this.creditMemory!!() + 1)
}

val betLevel: GameContextBoundIntGenerator = {
    betLevel
}

val overcomeBet: GameContextBoundIntGenerator = {
    AtLeast(betLevel() + 1)
}

val ownedCredits: GameContextBoundIntGenerator = {
    creditMemory!!
}

val pickedPlayerScore: GameContextBoundIntGenerator = {
    scoreMemory!!
}

val leaveAtLeastOnePlayerAside: GameContextBoundIntGenerator = {
    AtMost(players.size)
}

fun between(value1: GameContextBoundIntGenerator, value2: GameContextBoundIntGenerator): GameContextBoundIntGenerator = {
    Between(this.value1()(), this.value2()())
}

val enoughToReachPureSabacc: GameContextBoundIntGenerator = {
    Fixed(23 - scoreMemory!!())
}

val lessOrMoreThanEnoughToReachPureSabacc: GameContextBoundIntGenerator = {
    val scoreToReachPureSabacc = 23 - scoreMemory!!()
    if (scoreToReachPureSabacc >= 15) {
        Between(0, 15)
    } else {
        when (Random.nextBoolean()) {
            true -> Between(0, scoreToReachPureSabacc)
            false -> Between(scoreToReachPureSabacc, 15)
        }
    }
}

val lessThanEnoughToReachPureSabacc: GameContextBoundIntGenerator = {
    val scoreToReachPureSabacc = 23 - scoreMemory!!()
    Between(0, scoreToReachPureSabacc)
}

val moreThanEnoughToReachPureSabacc: GameContextBoundIntGenerator = {
    val scoreToReacPureSabacc = 23 - scoreMemory!!()
    Between(scoreToReacPureSabacc, 15)
}

val lessThanTheTie: GameContextBoundIntGenerator = {
    AtMost(drawValue!!())
}
