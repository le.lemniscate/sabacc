package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.helpers.generators.*
import io.lemniscat.sabacc.domain.helpers.mutators.*
import io.lemniscat.sabacc.domain.helpers.generators.AnyBut
import io.lemniscat.sabacc.domain.helpers.given
import io.lemniscat.sabacc.domain.helpers.on
import io.lemniscat.sabacc.domain.helpers.then
import org.junit.jupiter.api.Test

class StandTests {
    @Test
    fun `Game must be in Dealing phase in order to try to stand`() {
        given {
            phase(AnyBut("Dealing"))
        }.on {
            stand("current player")
        }.then {
            errorReceived("invalid state")
        }
    }

    @Test
    fun `Only the current player can try to stand`() {
        given {
            phase("Dealing")
        }.on {
            stand("any but current player")
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `Player must still be in game in order to stand`() {
        given {
            phase("Dealing")
            currentPlayer(fold)
        }.on {
            stand("current player")
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `If player stands, her hand should stay the same and game moves to next player`() {
        given {
            phase("Dealing")
            currentPlayer(withHandSize(anyHandSize))
        }.on {
            stand("current player")
        }.then {
            player("current player").handDidNotChange()
            playerIs("next player")
        }
    }

    @Test
    fun `If last player of the round stood, game moves to a new Betting phase`() {
        given {
            phase("Dealing")
            startedBy("next player")
        }.on {
            stand("current player")
        }.then {
            phaseIs("betting")
        }
    }

    @Test
    fun `Updated gaming table is stored in the repository`() {
        given {
            phase("Dealing")
        }.on {
            stand("current player")
        }.then {
            tableIsSaved()
        }
    }
}