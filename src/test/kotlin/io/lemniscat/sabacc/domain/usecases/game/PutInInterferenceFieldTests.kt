package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.helpers.generators.*
import io.lemniscat.sabacc.domain.helpers.generators.AnyBut
import io.lemniscat.sabacc.domain.helpers.generators.AnyOf
import io.lemniscat.sabacc.domain.helpers.given
import io.lemniscat.sabacc.domain.helpers.on
import io.lemniscat.sabacc.domain.helpers.then
import io.lemniscat.sabacc.domain.helpers.mutators.withHandSize
import org.junit.jupiter.api.Test

class PutInInterferenceFieldTests {
    @Test
    fun `Game can only be in Dealing, Calling or Betting phase to put a card in interference field`() {
        given {
            phase(AnyBut("Dealing", "Calling", "Betting"))
            currentPlayer(withHandSize(anyHandSize))
        }.on {
            putInInterferenceField("current player", "any card")
        }.then {
            errorReceived("invalid state")
        }
    }

    @Test
    fun `Card should be in player's hand in order to be put in interference field`() {
        given {
            phase(AnyOf("Dealing", "Calling", "Betting"))
            currentPlayer(withHandSize(anyHandSize))
        }.on {
            putInInterferenceField("current player", "any not owned card")
        }.then {
            errorReceived("card not owned")
        }
    }

    @Test
    fun `If all conditions are met, card should be put in player's interference field and the new gaming table is stored in the repository`() {
        given {
            phase(AnyOf("Dealing", "Calling", "Betting"))
            currentPlayer(withHandSize(anyHandSize))
        }.on {
            putInInterferenceField("current player", "any card")
        }.then {
            player("current player")
                    .handDoesNotContains("targeted card")
                    .interferenceFieldContains("targeted card")
            tableIsSaved()
        }
    }
}