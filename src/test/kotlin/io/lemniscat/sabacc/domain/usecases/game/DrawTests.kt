package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.helpers.mutators.fold
import io.lemniscat.sabacc.domain.helpers.generators.AnyBut
import io.lemniscat.sabacc.domain.helpers.given
import io.lemniscat.sabacc.domain.helpers.on
import io.lemniscat.sabacc.domain.helpers.then
import org.junit.jupiter.api.Test

class DrawTests {
    @Test
    fun `Game must be in Dealing phase in order to try to draw`() {
        given {
            phase(AnyBut("Dealing"))
        }.on {
            draw("any player")
        }.then {
            errorReceived("invalid state")
        }
    }

    @Test
    fun `Only the current player can try to draw`() {
        given {
            phase("Dealing")
        }.on {
            draw("any but current player")
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `Player must still be in game in order to draw`() {
        given {
            phase("Dealing")
            currentPlayer(fold)
        }.on {
            draw("current player")
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `If a player draws a card, her hand should have one card more and the deck should have one less`() {
        given {
            phase("Dealing")
        }.on {
            draw("current player")
        }.then {
            player("current player")
                    .handContains("first deck card")
            deckLost("first deck card")
        }
    }

    @Test
    fun `If last player of the round has drawn, game moves to a new Betting phase`() {
        given {
            phase("Dealing")
            startedBy("next player")
        }.on {
            draw("current player")
        }.then {
            phaseIs("betting")
        }
    }

    @Test
    fun `If there is not enough cards, player cannot draw`() {
        given {
            phase("Dealing")
            deck("is empty")
        }.on {
            draw("current player")
        }.then {
            errorReceived("not enough cards to deal")
        }
    }

    @Test
    fun `Updated gaming table is stored in the repository with next player set to the next player`() {
        given {
            phase("Dealing")
        }.on {
            draw("current player")
        }.then {
            playerIs("next player")
            tableIsSaved()
        }
    }
}