package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.helpers.generators.*
import io.lemniscat.sabacc.domain.helpers.mutators.fold
import io.lemniscat.sabacc.domain.helpers.generators.AnyBut
import io.lemniscat.sabacc.domain.helpers.given
import io.lemniscat.sabacc.domain.helpers.on
import io.lemniscat.sabacc.domain.helpers.then
import org.junit.jupiter.api.Test

class CallTests {
    @Test
    fun `Game must be in Betting phase in order to call`() {
        given {
            phase(AnyBut("Betting"))
        }.on {
            call("any player")
        }.then {
            errorReceived("invalid state")
        }
    }

    @Test
    fun `Only the current player can call the hand`() {
        given {
            phase("Betting")
            round(AtLeast(4))
        }.on {
            call("any but current player")
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `Players cannot call a hand during pot building phase`() {
        given {
            phase("Betting")
            round(AtMost(4))
        }.on {
            call("current player")
        }.then {
            errorReceived("pot building not over")
        }
    }

    @Test
    fun `Player must still be in game in order to call a hand`() {
        given {
            phase("Betting")
            round(AtLeast(4))
            currentPlayer(fold)
        }.on {
            call("current player")
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `After having called a hand, the game resume in Calling Phase with calling player being the current player`() {
        given {
            phase("Betting")
            round(AtLeast(4))
        }.on {
            call("current player")
        }.then {
            phaseIs("calling")
            playerIs("current player")
            callerIs("current player")
        }
    }

    @Test
    fun `Updated gaming table is stored in the repository with player still set to the current player`() {
        given {
            phase("Betting")
            round(AtLeast(4))
        }.on {
            call("current player")
        }.then {
            playerIs("current player")
            tableIsSaved()
        }
    }
}
