package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.helpers.generators.*
import io.lemniscat.sabacc.domain.helpers.mutators.*
import io.lemniscat.sabacc.domain.helpers.accessors.raiseAmount
import io.lemniscat.sabacc.domain.helpers.given
import io.lemniscat.sabacc.domain.helpers.on
import io.lemniscat.sabacc.domain.helpers.then
import org.junit.jupiter.api.Test

class RaiseBetTests {
    @Test
    fun `Game must be in Betting or Calling phase in order to try to raise a bet`() {
        given {
            phase(AnyBut("Betting", "Calling"))
        }.on {
            raiseBet("any player", AtLeast(0))
        }.then {
            errorReceived("invalid state")
        }
    }

    @Test
    fun `Only the current player can try to raise the bet`() {
        given {
            phase(AnyOf("Betting", "Calling"))
        }.on {
            raiseBet("any but current player", AtLeast(0))
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `Player must have enough credits to match her raise`() {
        given {
            phase(AnyOf("Betting", "Calling"))
            currentPlayer(withCredits(AtLeast(0)))
        }.on {
            raiseBet("current player", moreThanOwnedCredits)
        }.then {
            errorReceived("not enough credits")
        }
    }

    @Test
    fun `Player must bet more than the current bet in order to raise`() {
        given {
            phase(AnyOf("Betting", "Calling"))
            raisedBetTo(AtLeast(0))
            currentPlayer(canOvercomeBet)
        }.on {
            raiseBet("current player", betLevel)
        }.then {
            errorReceived("bet too low")
        }
    }

    @Test
    fun `Player must still be in game in order to raise a bet`() {
        given {
            phase(AnyOf("Betting", "Calling"))
            currentPlayer(canOvercomeBet, fold)
        }.on {
            raiseBet("current player", overcomeBet)
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `After a player has raised to X credits, she has X less credits and the main pot has X more credits`() {
        given {
            phase(AnyOf("Betting", "Calling"))
            currentPlayer(canOvercomeBet)
        }.on {
            raiseBet("current player", between(betLevel, ownedCredits))
        }.then {
            mainPotGained(raiseAmount)
            player("current player").lostCredits(raiseAmount)
        }
    }

    @Test
    fun `After a player has raised, Betting or Calling phase should take note of the new raiser`() {
        given {
            phase(AnyOf("Betting", "Calling"))
            currentPlayer(canOvercomeBet)
        }.on {
            raiseBet("current player", between(betLevel, ownedCredits))
        }.then {
            raiserIs("current player")
            currentBetIs(raiseAmount)
        }
    }

    @Test
    fun `Updated gaming table is stored in the repository with next player set to the next player`() {
        given {
            phase(AnyOf("Betting", "Calling"))
            currentPlayer(canOvercomeBet)
        }.on {
            raiseBet("current player", between(betLevel, ownedCredits))
        }.then {
            tableIsSaved()
            playerIs("next player")
        }
    }
}
