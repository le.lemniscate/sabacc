package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.helpers.generators.*
import io.lemniscat.sabacc.domain.helpers.mutators.*
import io.lemniscat.sabacc.domain.helpers.accessors.*
import io.lemniscat.sabacc.domain.helpers.given
import io.lemniscat.sabacc.domain.helpers.on
import io.lemniscat.sabacc.domain.helpers.then
import org.junit.jupiter.api.Test

class RevealCardsTests {
    @Test
    fun `Cards can only be revealed when in revealing phase`() {
        given {
            phase(AnyBut("Revealing"))
        }.on {
            revealCards()
        }.then {
            errorReceived("invalid state")
        }
    }

    @Test
    fun `If during revealing phase there is only one player left, jump to game over phase`() {
        given {
            phase("Revealing")
            pick("any player")
            allPlayersBut("picked")(outOfGame)
        }.on {
            revealCards()
        }.then {
            phaseIs("game over")
            player("picked").won()
        }
    }

    @Test
    fun `A player that hands scores more than 23 or less than 23 at the time the hand is called bombs out, paying an amount equals to the main pot into the sabacc pot`() {
        given {
            phase("Revealing")
            pick("any player")
            allPlayersBut("picked")(withHand(HandOfSize(Fixed(1))))
            pickedPlayer(
                    canMatchMainPot,
                    withHand(MoreOrLessThan23())
            )
        }.on {
            revealCards()
        }.then {
            player("picked")
                    .hasBombedOut()
                    .lostCredits(mainPotAmount)
            sabaccPotGained(mainPotAmount)
        }
    }

    @Test
    fun `If a player that bombs out cannot pay the amount, she looses everything to the sabacc pot and is out of money`() {
        given {
            phase("Revealing")
            pick("any player")
            allPlayersBut("picked")(withHand(HandOfSize(Fixed(1))))
            pickedPlayer(
                    cannotMatchMainPot,
                    withHand(MoreOrLessThan23())
            )
        }.on {
            revealCards()
        }.then {
            player("picked")
                    .isOutOfMoney()
                    .lostCredits(allOwnedCredits)
            sabaccPotGained(allOwnedCredits)
        }
    }

    @Test
    fun `A player that hands scores exactly 0 at the time the hand is called bombs out, paying an amount equals to the main pot into the sabacc pot`() {
        given {
            phase("Revealing")
            pick("any player")
            allPlayersBut("picked")(withHand(HandOfSize(Fixed(1))))
            pickedPlayer(
                    canMatchMainPot,
                    withHand(ScoringZero())
            )
        }.on {
            revealCards()
        }.then {
            player("picked")
                    .hasBombedOut()
                    .lostCredits(mainPotAmount)
            sabaccPotGained(mainPotAmount)
        }
    }

    @Test
    fun `A hand that scores 23 (Pure Sabacc) wins if all other hands scores between -23 and 22 with not Idiot's array, gaining content of both pots`() {
        given {
            phase("Revealing")
            pick("any player")
            pickedPlayer(withHand(PureSabaccHand()))
            allPlayersBut("picked")(withHand(NormalHand()))
        }.on {
            revealCards()
        }.then {
            phaseIs("game over")
            player("picked")
                    .won()
                    .andGainedCredits("main pot", "sabacc pot")
            mainPotIs(empty)
            sabaccPotIs(empty)
        }
    }

    @Test
    fun `If everybody bombs out, game is over and the main pot goes to the sabacc pot`() {
        given {
            phase("Revealing")
            allPlayers(withHand(MoreOrLessThan23()))
        }.on {
            revealCards()
        }.then {
            phaseIs("game over")
            noWinner()
            mainPotIs(empty)
            sabaccPotGained(mainPotAmount, allPlayerContributedAnAmountEqualsToMainPotOrAllTheirCredit)
        }
    }

    @Test
    fun `A hand that scores -23 (Pure Sabacc) wins if all other hands scores between -22 and 22 with no Idiot's array, and takes both pots`() {
        given {
            phase("Revealing")
            pick("any player")
            allPlayersBut("picked")(withHand(NormalHand()))
            pickedPlayer(withHand(PureSabaccHand()))
        }.on {
            revealCards()
        }.then {
            phaseIs("game over")
            player("picked")
                    .won()
                    .andGainedCredits("main pot", "sabacc pot")
            mainPotIs(empty)
            sabaccPotIs(empty)
        }
    }

    @Test
    fun `A hand that scores 23 wins against a hand scoring -23, winning both pots`() {
        given {
            phase("Revealing")
            pick("any player")
            allPlayersBut("picked")(withHand(NegativePureSabaccHand()))
            pickedPlayer(withHand(PureSabaccHand()))
        }.on {
            revealCards()
        }.then {
            phaseIs("game over")
            player("picked")
                    .won()
                    .andGainedCredits("main pot", "sabacc pot")
            mainPotIs(empty)
            sabaccPotIs(empty)
        }
    }

    @Test
    fun `When nobody reaches Pure Sabacc or Idiot's Array and there is no tie, the closest to 23 (in absolute) wins the main pot`() {
        given {
            phase("Revealing")
            pick("any player")
            pickedPlayer(withScore(PositiveOrNegativeBetween(2, 23)))
            allPlayersBut("picked")(withScoreLessThat(pickedPlayerScore))
        }.on {
            revealCards()
        }.then {
            phaseIs("game over")
            player("picked")
                    .won()
                    .andGainedCredits("main pot")
            mainPotIs(empty)
        }
    }

    @Test
    fun `When nobody reaches Pure Sabacc or Idiot's Array and there is a tie between a negative and a positive score, the positive wins`() {
        given {
            phase("Revealing")
            pick("any player")
            pickedPlayer(withScore(Between(1, 23)))
            allPlayersBut("picked")(withScoreLessThat(pickedPlayerScore))
            anyButPickedPlayer(opositeThanPickedScore)
        }.on {
            revealCards()
        }.then {
            phaseIs("game over")
            player("picked")
                    .won()
                    .andGainedCredits("main pot")
            mainPotIs(empty)
        }
    }

    @Test
    fun `Idiot's array wins against pure sabacc`() {
        given {
            phase("Revealing")
            pick("any player")
            pickedPlayer(withHand(IdiotArrayHand()))
            allPlayersBut("picked")(withHand(NormalHand()))
            anyButPickedPlayer(withHand(PureSabaccHand()))
        }.on {
            revealCards()
        }.then {
            phaseIs("game over")
            player("picked")
                    .won()
                    .andGainedCredits("main pot", "sabacc pot")
            mainPotIs(empty)
            sabaccPotIs(empty)
        }
    }

    @Test
    fun `Idiot's array wins against -23`() {
        given {
            phase("Revealing")
            pick("any player")
            pickedPlayer(withHand(IdiotArrayHand()))
            allPlayersBut("picked")(withHand(NormalHand()))
            anyButPickedPlayer(withHand(NegativePureSabaccHand()))
        }.on {
            revealCards()
        }.then {
            phaseIs("game over")
            player("picked")
                    .won()
                    .andGainedCredits("main pot", "sabacc pot")
            mainPotIs(empty)
            sabaccPotIs(empty)
        }
    }

    @Test
    fun `Idiot's array wins against any normal hand`() {
        given {
            phase("Revealing")
            pick("any player")
            pickedPlayer(withHand(IdiotArrayHand()))
            allPlayersBut("picked")(withHand(NormalHand()))
        }.on {
            revealCards()
        }.then {
            phaseIs("game over")
            player("picked")
                    .won()
                    .andGainedCredits("main pot", "sabacc pot")
            mainPotIs(empty)
            sabaccPotIs(empty)
        }
    }

    @Test
    fun `Winner takes the content of the main pot after Bombed Out players contributed to it`() {
        given {
            phase("Revealing")
            pick("any player")
            pickedPlayer(withHand(IdiotArrayHand()))
            allPlayersBut("picked")(withHand(MoreOrLessThan23()))
        }.on {
            revealCards()
        }.then {
            phaseIs("game over")
            player("picked")
                    .won()
                    .andGainedCredits(
                            mainPotAmount,
                            sabaccPotAmount,
                            allPlayerButThePickedOneContributedAnAmountEqualsToMainPotOrAllTheirCredit
                    )
            mainPotIs(empty)
            sabaccPotIs(empty)
        }
    }

    @Test
    fun `If there is a tie, games go to Sudden Demise phase`() {
        given {
            phase("Revealing")
            pick("any player")
            pickedPlayer(withScore(Between(1, 23)))
            allPlayersBut("picked")(withScoreLessThat(pickedPlayerScore))
            anyButPickedPlayer(equalToPickedScore)
        }.on {
            revealCards()
        }.then {
            phaseIs("sudden demise")
        }
    }

    @Test
    fun `Updated gaming table is stored in the repository`() {
        given {
            phase("Revealing")
            allPlayers(withHand(NormalHand()))
        }.on {
            revealCards()
        }.then {
            tableIsSaved()
        }
    }
}