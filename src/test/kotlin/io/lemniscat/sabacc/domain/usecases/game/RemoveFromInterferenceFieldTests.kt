package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.helpers.generators.*
import io.lemniscat.sabacc.domain.helpers.mutators.*
import io.lemniscat.sabacc.domain.helpers.generators.AnyBut
import io.lemniscat.sabacc.domain.helpers.generators.AnyOf
import io.lemniscat.sabacc.domain.helpers.given
import io.lemniscat.sabacc.domain.helpers.on
import io.lemniscat.sabacc.domain.helpers.then
import org.junit.jupiter.api.Test

class RemoveFromInterferenceFieldTests {

    @Test
    fun `Game can only be in Dealing, Calling or Betting phase to remove a card from interference field`() {
        given {
            phase(AnyBut("Dealing", "Calling", "Betting"))
            currentPlayer(withInterferenceFieldSize(anyHandSize))
        }.on {
            removeFromInterferenceField("current player", "any card")
        }.then {
            errorReceived("invalid state")
        }
    }

    @Test
    fun `Card should be in player's interference field in order to be removed`() {
        given {
            phase(AnyOf("Dealing", "Calling", "Betting"))
            currentPlayer(withInterferenceFieldSize(anyHandSize))
        }.on {
            removeFromInterferenceField("current player", "any not interfered card")
        }.then {
            errorReceived("card not in interference field")
        }
    }

    @Test
    fun `If all conditions are met, card should be put back in player's hand and the new gaming table is stored in the repository`() {
        given {
            phase(AnyOf("Dealing", "Calling", "Betting"))
            currentPlayer(withInterferenceFieldSize(anyHandSize))
        }.on {
            removeFromInterferenceField("current player", "any card")
        }.then {
            player("current player")
                    .interferenceFieldDoesNotContain("targeted card")
                    .handContains("targeted card")
            tableIsSaved()
        }
    }

}
