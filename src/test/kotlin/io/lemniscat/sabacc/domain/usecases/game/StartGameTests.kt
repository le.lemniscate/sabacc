package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.helpers.generators.*
import io.lemniscat.sabacc.domain.helpers.accessors.exactly
import io.lemniscat.sabacc.domain.helpers.accessors.numberOfPlayers
import io.lemniscat.sabacc.domain.helpers.accessors.oneLessPlayer
import io.lemniscat.sabacc.domain.helpers.accessors.perPlayer
import io.lemniscat.sabacc.domain.helpers.givenLobby
import io.lemniscat.sabacc.domain.helpers.mutators.broke
import io.lemniscat.sabacc.domain.helpers.mutators.inGame
import io.lemniscat.sabacc.domain.helpers.on
import io.lemniscat.sabacc.domain.helpers.then
import org.junit.jupiter.api.Test

class StartGameTests {
    @Test
    fun `Game can only start with at least 2 people in the Lobby`() {
        givenLobby {
            joinedBy(AtMost(2))
        }.on {
            startGame()
        }.then {
            errorReceived("not enough players")
        }
    }

    @Test
    fun `Game can only start with less than 8 people in the Lobby`() {
        givenLobby {
            joinedBy(AtLeast(9))
        }.on {
            startGame()
        }.then {
            errorReceived("too many players")
        }
    }

    @Test
    fun `Game can only start over if between 2 and 8 players are still in game`() {
        givenLobby {
            joinedBy(Between(2, 8))
            all(broke)
            one(inGame)
        }.on {
            startGame()
        }.then {
            errorReceived("not enough players")
        }
    }

    @Test
    fun `Game starts when there is between 2 and 8 players and is then saved in the repository`() {
        givenLobby {
            joinedBy(Between(2, 8))
        }.on {
            startGame()
        }.then {
            tableIsSaved()
        }
    }

    @Test
    fun `At the start of the game, everybody gives 1 credit to game pot and sabacc pot`() {
        givenLobby {
            joinedBy(Between(2, 8))
        }.on {
            startGame()
        }.then {
            mainPotIs(numberOfPlayers)
            sabaccPotIs(numberOfPlayers)
            players().lostCredits(exactly(2))
        }
    }

    @Test
    fun `At the start of a game, a user with less than 2 credits is excluded`() {
        givenLobby {
            joinedBy(Between(3, 8))
            one(broke)
        }.on {
            startGame()
        }.then {
            numberOfPlayersIs(oneLessPlayer)
        }
    }

    @Test
    fun `At the start of a game, the deck is shuffled and two cards are dealt to each player`() {
        givenLobby {
            joinedBy(Between(3, 8))
        }.on {
            startGame()
        }.then {
            players().hasHandSize(Fixed(2))
            deckLost(perPlayer(2))
        }
    }

    @Test
    fun `After a game is started, its phase is the betting phase`() {
        givenLobby {
            joinedBy(Between(3, 8))
        }.on {
            startGame()
        }.then {
            phaseIs("betting")
        }
    }

}