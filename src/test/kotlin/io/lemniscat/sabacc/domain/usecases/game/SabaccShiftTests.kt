package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.helpers.generators.*
import io.lemniscat.sabacc.domain.helpers.mutators.*
import io.lemniscat.sabacc.domain.helpers.generators.AnyBut
import io.lemniscat.sabacc.domain.helpers.generators.AnyOf
import io.lemniscat.sabacc.domain.helpers.given
import io.lemniscat.sabacc.domain.helpers.on
import io.lemniscat.sabacc.domain.helpers.then
import org.junit.jupiter.api.Test

class SabaccShiftTests {
    @Test
    fun `Game can only be in Dealing, Calling or Betting phase to shift`() {
        given {
            phase(AnyBut("Dealing", "Calling", "Betting"))
        }.on {
            sabaccShift()
        }.then {
            errorReceived("invalid state")
        }
    }

    @Test
    fun `After a shift all players have the exact same amount of cards in hand`() {
        given {
            phase(AnyOf("Dealing", "Calling", "Betting"))
            allPlayers(withHandSize(littleHand))
        }.on {
            sabaccShift()
        }.then {
            players().handSizeDidNotChange()
        }
    }

    @Test
    fun `After a shift all players should get random new cards while interference field should be untouched`() {
        given {
            phase(AnyOf("Dealing", "Calling", "Betting"))
            allPlayers(
                    withHandSize(littleHand),
                    withInterferenceFieldSize(anyHandSize)
            )
        }.on {
            sabaccShift()
        }.then {
            players()
                    .interferenceFieldDidNotChange()
                    .haveDrawnTopOfTheDeck()
            tableIsSaved()
        }
    }
}