package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.helpers.generators.*
import io.lemniscat.sabacc.domain.helpers.mutators.*
import io.lemniscat.sabacc.domain.helpers.generators.AnyBut
import io.lemniscat.sabacc.domain.helpers.given
import io.lemniscat.sabacc.domain.helpers.on
import io.lemniscat.sabacc.domain.helpers.then
import org.junit.jupiter.api.Test

class FoldTests {
    @Test
    fun `Game must be in Betting phase in order to try to fold`() {
        given {
            phase(AnyBut("Betting"))
        }.on {
            fold("any player")
        }.then {
            errorReceived("invalid state")
        }
    }

    @Test
    fun `Only the current player can try to fold`() {
        given {
            phase("Betting")
        }.on {
            fold("any but current player")
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `Player must still be in game in order to fold`() {
        given {
            phase("Betting")
            currentPlayer(fold)
        }.on {
            fold("current player")
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `If player has fold and the next player is the one who raised, game should move to Dealing phase`() {
        given {
            phase("Betting")
            nextPlayer(raisedBet)
        }.on {
            fold("current player")
        }.then {
            phaseIs("dealing")
            playerIs("next player")
        }
    }

    @Test
    fun `If player is the last one to fold but one, the game ends and the last player wins the main pot and the main pot is empty`() {
        given {
            phase("Betting")
            numberOfPlayers(Between(3, 9))
            stillInGame("current player", pick("any but current player"))
        }.on {
            fold("current player")
        }.then {
            phaseIs("game over")
            winnerIs("picked")
                    .andGainedCredits("main pot")
            mainPotContains(0)
        }
    }

    @Test
    fun `Updated gaming table is stored in the repository with next player set to the next player`() {
        given {
            phase("Betting")
        }.on {
            fold("current player")
        }.then {
            playerIs("next player")
            tableIsSaved()
        }
    }
}
