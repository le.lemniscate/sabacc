package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.helpers.generators.*
import io.lemniscat.sabacc.domain.helpers.mutators.*
import io.lemniscat.sabacc.domain.helpers.generators.AnyBut
import io.lemniscat.sabacc.domain.helpers.given
import io.lemniscat.sabacc.domain.helpers.on
import io.lemniscat.sabacc.domain.helpers.then
import org.junit.jupiter.api.Test

class TradeTests {
    @Test
    fun `Game must be in Dealing phase in order to try to trade`() {
        given {
            phase(AnyBut("Dealing"))
            currentPlayer(withHandSize(anyHandSize))
        }.on {
            trade("current player", "any card")
        }.then {
            errorReceived("invalid state")
        }
    }

    @Test
    fun `Only the current player can try to trade`() {
        given {
            phase("Dealing")
            pick("any but current player")
            pickedPlayer(withHandSize(anyHandSize))
        }.on {
            trade("picked", "any card")
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `Player must still be in game in order to trade`() {
        given {
            phase("Dealing")
            currentPlayer(withHandSize(anyHandSize), fold)
        }.on {
            trade("current player", "any card")
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `Player must own the card in order to trade it`() {
        given {
            phase("Dealing")
            currentPlayer(withHandSize(anyHandSize))
        }.on {
            trade("current player", "any not owned card")
        }.then {
            errorReceived("card not owned")
        }
    }

    @Test
    fun `If player trades a card, her hand should have one new card and lost the traded card`() {
        given {
            phase("Dealing")
            currentPlayer(withHandSize(anyHandSize))
        }.on {
            trade("current player", "any card")
        }.then {
            player("current player")
                    .handDoesNotContains("targeted card")
                    .handContains("first deck card")
        }
    }

    @Test
    fun `If player trades a card, the traded card should be at the bottom of the deck`() {
        given {
            phase("Dealing")
            currentPlayer(withHandSize(anyHandSize))
        }.on {
            trade("current player", "any card")
        }.then {
            deckLastCardIs("targeted card")
        }
    }

    @Test
    fun `If last player of the round has traded, game moves to a new Betting phase with the next player being the new current player`() {
        given {
            phase("Dealing")
            currentPlayer(withHandSize(anyHandSize))
            startedBy("next player")
        }.on {
            trade("current player", "any card")
        }.then {
            playerIs("next player")
            phaseIs("betting")
        }
    }

    @Test
    fun `If there is not enough cards, player cannot trade`() {
        given {
            phase("Dealing")
            currentPlayer(withHandSize(anyHandSize))
            deck("is empty")
        }.on {
            trade("current player", "any card")
        }.then {
            errorReceived("not enough cards to deal")
        }
    }

    @Test
    fun `Updated gaming table is stored in the repository`() {
        given {
            phase("Dealing")
            startedBy("any player")
            currentPlayer(withHandSize(anyHandSize))
        }.on {
            trade("current player", "any card")
        }.then {
            playerIs("next player")
            tableIsSaved()
        }
    }
}