package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.helpers.generators.*
import io.lemniscat.sabacc.domain.helpers.mutators.*
import io.lemniscat.sabacc.domain.helpers.generators.AnyBut
import io.lemniscat.sabacc.domain.helpers.generators.AnyOf
import io.lemniscat.sabacc.domain.helpers.given
import io.lemniscat.sabacc.domain.helpers.on
import io.lemniscat.sabacc.domain.helpers.then
import org.junit.jupiter.api.Test

class MatchBetTests {
    @Test
    fun `Game must be in Betting or Calling phase in order to try to match a bet`() {
        given {
            phase(AnyBut("Betting", "Calling"))
        }.on {
            matchBet("any player")
        }.then {
            errorReceived("invalid state")
        }
    }

    @Test
    fun `Only the current player can try to match the bet`() {
        given {
            phase(AnyOf("Betting", "Calling"))
        }.on {
            matchBet("any but current player")
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `Player must have enough credits in order to try to match a bet`() {
        given {
            phase(AnyOf("Betting", "Calling"))
            raisedBetTo(AtLeast(1))
            currentPlayer(cannotMatchBet)
        }.on {
            matchBet("current player")
        }.then {
            errorReceived("not enough credits")
        }
    }

    @Test
    fun `Player must still be in game in order to match a bet`() {
        given {
            phase(AnyOf("Betting", "Calling"))
            currentPlayer(fold)
        }.on {
            matchBet("current player")
        }.then {
            errorReceived("not player turn")
        }
    }

    @Test
    fun `After a player has match a bet of X credits, she has X less credits and the main pot has X more credits`() {
        given {
            phase(AnyOf("Betting", "Calling"))
            raisedBetTo(Between(1, 1000))
            currentPlayer(canMatchBet)
        }.on {
            matchBet("current player")
        }.then {
            mainPotGained("betting value")
            player("current player").lostCredits("betting value")
        }
    }

    @Test
    fun `If the player is the last to match the current bet, game moves to Dealing phase with next player as the current one and number of rounds is increased`() {
        given {
            phase("Betting")
            startedBy("next player")
            raisedBetTo(Between(1, 1000))
            currentPlayer(canMatchBet)
        }.on {
            matchBet("current player")
        }.then {
            phaseIs("dealing")
            playerIs("next player")
            roundIncreased()
        }
    }

    @Test
    fun `If the player is the last to match the current bet while in Calling Phase, game move to winner check`() {
        given {
            phase("Calling")
            startedBy("next player")
            raisedBetTo(Between(1, 1000))
            currentPlayer(canMatchBet)
        }.on {
            matchBet("current player")
        }.then {
            phaseIs("revealing")
        }
    }

    @Test
    fun `Updated gaming table is stored in the repository`() {
        given {
            phase(AnyOf("Betting", "Calling"))
            raisedBetTo(Between(1, 1000))
            currentPlayer(canMatchBet)
        }.on {
            matchBet("current player")
        }.then {
            tableIsSaved()
        }
    }
}