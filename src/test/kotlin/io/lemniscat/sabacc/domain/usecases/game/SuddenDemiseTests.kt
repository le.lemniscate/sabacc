package io.lemniscat.sabacc.domain.usecases.game

import io.lemniscat.sabacc.domain.helpers.generators.*
import io.lemniscat.sabacc.domain.helpers.mutators.*
import io.lemniscat.sabacc.domain.helpers.accessors.mainPotAmount
import io.lemniscat.sabacc.domain.helpers.accessors.numberOfPickedPlayers
import io.lemniscat.sabacc.domain.helpers.given
import io.lemniscat.sabacc.domain.helpers.on
import io.lemniscat.sabacc.domain.helpers.then
import org.junit.jupiter.api.Test

class SuddenDemiseTests {
    @Test
    fun `Sudden demise can can only be applied when in sudden demise phase`() {
        given {
            phase(AnyBut("SuddenDemise"))
        }.on {
            suddenDemise()
        }.then {
            errorReceived("invalid state")
        }
    }

    @Test
    fun `Each player still involved in the game are dealt one card`() {
        given {
            pick(leaveAtLeastOnePlayerAside)
            startSuddenDemiseWith("picked")
        }.on {
            suddenDemise()
        }.then {
            players().gainedCard(Fixed(1))
        }
    }

    @Test
    fun `The player whose score is the best wins the game and the main pot`() {
        given {
            pick(leaveAtLeastOnePlayerAside)
            startSuddenDemiseWith("picked")
        }.on {
            suddenDemise()
        }.then {
            phaseIs("game over")
            winner()
                    .hasTheBestScore()
                    .andGainedCredits("main pot")
            loosers()
                    .creditsDidNotChange()
        }
    }

    @Test
    fun `The player whose score is the best wins the game and both pot if it is with a Pure Sabacc and the state is stored in the repository`() {
        given {
            startSuddenDemiseWith("all")
            allPlayers(withScore(Between(8, 23)))
            // In sudden demise, everybody has the same hand, and we can draw at most a score of 15
            // So for a pure sabacc, we need at least 8 at first: 8+15=23
            whenDrawing {
                onePlayerWillGet(enoughToReachPureSabacc)
                whileOtherWillGet(lessOrMoreThanEnoughToReachPureSabacc)
            }
        }.on {
            suddenDemise()
        }.then {
            phaseIs("game over")
            winner()
                    .hasTheBestScore()
                    .andGainedCredits("main pot", "sabacc pot")
        }
    }

    @Test
    fun `If it is a Tie, players involved in the Tie stays in the next round of Sudden Demise`() {
        given {
            startSuddenDemiseWith("all")
            allPlayers(withScore(Between(1, 22)))
            // In sudden demise, everybody has the same hand, and we can draw at most a score of 15
            // So for a pure sabacc, we need at least 8 at first: 8+15=23
            whenDrawing {
                playersWillBeTied("some")
                whileOtherWillGet(lessThanTheTie)
            }
        }.on {
            suddenDemise()
        }.then {
            phaseIs("sudden demise")
            playersInvolved(numberOfPickedPlayers)
        }
    }

    @Test
    fun `If players involved in the Sudden Demise bomb out, they do not pay to the sabacc pot`() {
        given {
            startSuddenDemiseWith("all")
            allPlayers(withScore(Fixed(22)))
            whenDrawing {
                onePlayerWillGet(enoughToReachPureSabacc)
                whileOtherWillGet(moreThanEnoughToReachPureSabacc)
            }
        }.on {
            suddenDemise()
        }.then {
            phaseIs("game over")
            loosers()
                    .creditsDidNotChange()
        }
    }

    @Test
    fun `If all players involved in the Sudden Demise bomb out, the main pot goes to the player with the best score who has not bombed out`() {
        given {
            startSuddenDemiseWith("all")
            allPlayers(withScore(Between(9, 20)))
            whenDrawing {
                onePlayerWillGet(lessThanEnoughToReachPureSabacc)
                whileOtherWillGet(moreThanEnoughToReachPureSabacc)
            }
        }.on {
            suddenDemise()
        }.then {
            phaseIs("game over")
            winner()
                    .andGainedCredits("main pot")
        }
    }

    @Test
    fun `If there is not enough cards for all players involved in the Sudden Demise to draw, the main pot goes to the sabacc pot`() {
        given {
            startSuddenDemiseWith("all")
            deck("is empty")
        }.on {
            suddenDemise()
        }.then {
            phaseIs("game over")
            noWinner()
            sabaccPotGained(mainPotAmount)
        }
    }

    @Test
    fun `If all players involved in the Sudden Demise bomb out and there is a tie for the second best score, the main pot goes to the sabacc pot`() {
        given {
            numberOfPlayers(Between(4, 9))
            startSuddenDemiseWith("all but at least two")
            allPlayers(withScore(Between(9, 20)))
            whenDrawing {
                allPlayerWillGet(moreThanEnoughToReachPureSabacc)
            }
        }.on {
            suddenDemise()
        }.then {
            phaseIs("game over")
            noWinner()
            sabaccPotGained(mainPotAmount)
        }
    }

    @Test
    fun `If all players involved in the Sudden Demise bomb out and there is no other player still in game the main pot goes to the sabacc pot`() {
        given {
            numberOfPlayers(AtLeast(3))
            startSuddenDemiseWith("all")
            allPlayers(withScore(Between(9, 20)))
            whenDrawing {
                allPlayerWillGet(moreThanEnoughToReachPureSabacc)
            }
        }.on {
            suddenDemise()
        }.then {
            phaseIs("game over")
            noWinner()
            sabaccPotGained(mainPotAmount)
        }
    }

}